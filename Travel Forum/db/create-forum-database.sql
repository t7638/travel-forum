CREATE DATABASE IF NOT EXISTS `forum`
USE `forum`

create table avatar
(
    avatar_id int auto_increment
        primary key,
    url       varchar(300) null,
    constraint avatar_avatar_id_uindex
        unique (avatar_id)
);

create table tag_types
(
    tag_id   int auto_increment
        primary key,
    tag_name varchar(20) not null,
    constraint tags_tag_id_uindex
        unique (tag_id),
    constraint tags_tag_name_uindex
        unique (tag_name)
);

create table users
(
    user_id    int auto_increment
        primary key,
    first_name varchar(32)          not null,
    last_name  varchar(32)          not null,
    email      varchar(50)          not null,
    username   varchar(50)          not null,
    password   varchar(50)          not null,
    is_blocked tinyint(1) default 0 not null,
    is_admin   tinyint(1) default 0 not null,
    is_deleted tinyint(1) default 0 not null,
    avatar_id  int        default 1 null,
    constraint users_email_uindex
        unique (email),
    constraint users_user_id_uindex
        unique (user_id),
    constraint users_username_uindex
        unique (username),
    constraint users_avatar_avatar_id_fk
        foreign key (avatar_id) references avatar (avatar_id)
);

create table comments
(
    comment_id    int auto_increment
        primary key,
    content       text not null,
    author_id     int  not null,
    creation_date date not null,
    constraint comments_comment_id_uindex
        unique (comment_id),
    constraint comments_users_user_id_fk
        foreign key (author_id) references users (user_id)
);

create table phone_numbers
(
    phone_id int auto_increment
        primary key,
    number   varchar(30) null,
    user_id  int         null,
    constraint phone_numbers_phone_id_uindex
        unique (phone_id),
    constraint phone_numbers_phone_number_uindex
        unique (number),
    constraint phone_numbers_user_id_uindex
        unique (user_id),
    constraint phone_numbers_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table posts
(
    post_id       int auto_increment
        primary key,
    author_id     int                  not null,
    title         varchar(64)          not null,
    content       text                 not null,
    creation_date date                 not null,
    is_deleted    tinyint(1) default 0 not null,
    constraint posts_post_id_uindex
        unique (post_id),
    constraint posts_users_user_id_fk
        foreign key (author_id) references users (user_id)
);

create table commented_posts
(
    post_id    int not null,
    comment_id int not null,
    constraint commented_posts_comments_comment_id_fk
        foreign key (comment_id) references comments (comment_id),
    constraint commented_posts_posts_post_id_fk
        foreign key (post_id) references posts (post_id)
);

create table reactions
(
    reaction_id   int auto_increment
        primary key,
    user_id       int        not null,
    reaction_type tinyint(1) not null,
    constraint reactions_reaction_id_uindex
        unique (reaction_id),
    constraint reactions_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table reacted_posts
(
    post_id     int not null,
    reaction_id int not null,
    constraint reacted_posts_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint reacted_posts_reactions_reaction_id_fk
        foreign key (reaction_id) references reactions (reaction_id)
);

create table tagged_posts
(
    post_id int not null,
    tag_id  int not null,
    constraint tagged_posts_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint tagged_posts_tag_types_tag_id_fk
        foreign key (tag_id) references tag_types (tag_id)
);

