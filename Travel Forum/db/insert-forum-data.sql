-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table forum.avatar: ~3 rows (approximately)
/*!40000 ALTER TABLE `avatar` DISABLE KEYS */;
INSERT INTO `avatar` (`avatar_id`, `url`) VALUES
                                              (1, 'https://www.seekpng.com/png/full/966-9665493_my-profile-icon-blank-profile-image-circle.png'),
                                              (2, 'https://www.kindpng.com/picc/m/78-785975_icon-profile-bio-avatar-person-symbol-chat-icon.png'),
                                              (3, 'https://www.pinclipart.com/picdir/big/78-780477_about-us-avatar-icon-red-png-clipart.png');
/*!40000 ALTER TABLE `avatar` ENABLE KEYS */;

-- Dumping data for table forum.commented_posts: ~14 rows (approximately)
/*!40000 ALTER TABLE `commented_posts` DISABLE KEYS */;
INSERT INTO `commented_posts` (`post_id`, `comment_id`) VALUES
                                                            (3, 2),
                                                            (3, 3),
                                                            (3, 2),
                                                            (3, 3),
                                                            (5, 4),
                                                            (2, 4),
                                                            (2, 8),
                                                            (5, 11),
                                                            (7, 12),
                                                            (4, 13),
                                                            (9, 14),
                                                            (2, 15),
                                                            (2, 16),
                                                            (6, 17);
/*!40000 ALTER TABLE `commented_posts` ENABLE KEYS */;

-- Dumping data for table forum.comments: ~11 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`comment_id`, `content`, `author_id`, `creation_date`) VALUES
                                                                                   (2, 'What kind of equipment do is required?', 5, '2022-02-25'),
                                                                                   (3, 'Deleted by author.', 3, '2022-02-26'),
                                                                                   (4, 'Deleted by admin.', 3, '2022-03-04'),
                                                                                   (8, 'Thanks you a lot for the fast reply!', 3, '2022-03-04'),
                                                                                   (11, 'Hey, you should visit Gjirokastër in Albaniq and Cotor Bay in Montenegro', 8, '2022-03-26'),
                                                                                   (12, 'This would be a lot of wolking to go to the seaside! Good luck', 10, '2022-03-27'),
                                                                                   (13, 'What about a knife?', 2, '2022-03-27'),
                                                                                   (14, 'You don\'t need to be nervous Chicago is a good place also you have a wonderful experience explore many new things.', 4, '2022-03-27'),
                                                                                   (15, 'Deleted by author.', 11, '2022-03-27'),
                                                                                   (16, 'Summer is the normal season for hiking. Winter is good for traverses, ice and mixed climbing.', 9, '2022-03-27'),
                                                                                   (17, 'Summer in my opinion.', 2, '2022-03-27');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping data for table forum.phone_numbers: ~2 rows (approximately)
/*!40000 ALTER TABLE `phone_numbers` DISABLE KEYS */;
INSERT INTO `phone_numbers` (`phone_id`, `number`, `user_id`) VALUES
                                                                  (5, '0888888888', 2),
                                                                  (7, '0885635487', 4);
/*!40000 ALTER TABLE `phone_numbers` ENABLE KEYS */;

-- Dumping data for table forum.posts: ~13 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`post_id`, `author_id`, `title`, `content`, `creation_date`, `is_deleted`) VALUES
                                                                                                    (2, 2, 'Hiking in Stara Planina', 'Stara Planina is notorious for its bad weather, especially the strong winds and the fog. The exposed parts are avalanche-risky, so you should always follow the winter markings - metal rods pained in yellow and black.', '2022-01-02', 0),
                                                                                                    (3, 3, 'Equipment for camping', 'The list of essential camping gear is as follows:\nMap, Compass, Sunglasses, sun cream and a sun hat,\nSpare warm clothing, Headlamp and/or handheld torch, First-aid kit,\nFirestarter, Matches, Knife, More food and water than you need', '2022-01-11', 0),
                                                                                                    (4, 2, 'Something to bring in a mountain', 'Hiking Boots. Base layer: Mid Layer (optional) Light-weight Jacket /Down. Rain jacket.', '2022-01-18', 0),
                                                                                                    (5, 7, 'Road trip in Albania and Montenegro', 'Hello, I\'m planing to travel for two weeks from Sofia, through Macedonia, Albania, Montenegro and back. Do you have any ideas for the places I should visit?', '2022-02-20', 0),
                                                                                                    (6, 8, 'Travel to Patagonia', 'Hey, travelers, \r\nMy dream is to travel to Patagonia. Finally I have time and money to do it! So my question is which season is the best for such adventure?', '2022-02-21', 0),
                                                                                                    (7, 9, 'Kom-Emine hiking', 'Hey, does any one of you try to make it? How long is the trip? Is there huts?', '2022-02-22', 0),
                                                                                                    (8, 10, 'Sailing in Greece', 'Hey, I\'m looking for crew for 1 month sailing from Varna to scandinavian fjords. I need 4 people with at least basic knowledge.', '2022-02-27', 0),
                                                                                                    (9, 11, 'First time in a big city (Chicago)', 'Hello I\'m new here and I need some advice. Im planning a trip to Chicago on April 2022. I have booked my hotel which will be in the downtown area of the city. I\'m excited but I\'m very nervous because I\'ve never been to a big city like Chicago. I grew up in a small town and went to college in a small town then moved to a small city with a population of about 200,000 for work which is where I\'ve been living since 2016. This was within the same state which is in the U.S midwest. I booked this trip because I want to get out of my comfort zone. (I\'ve never been outside my state) and I was inspired from watching YouTube videos on this.\r\nIm looking for advice on how to navigate a big city like Chicago. I plan to see some of the attractions and use the public transportation system there. So people from Chicago what advice would you give a small town person visiting your city.', '2022-03-01', 0),
                                                                                                    (10, 11, 'Titleeeeeeeeeeeeeeeeeee', 'TitleeeeeeeeeeeeeeeeeeeTitleeeeeeeeeeeeeeeeeeeTitleeeeeeeeeeeeeeeeeeeTitleeeeeeeeeeeeeeeeeee', '2022-03-02', 1),
                                                                                                    (11, 2, 'Around The World in 180 days January 2023 to June 2023', 'Ahoy, mates. Going to do the trip of a lifetime with wife of 36 years. Working on the Travel planner. Have a few questions. Is there a way to save or export the file of map onto my computer? Can I print out the finished map?.\r\nThanks.\r\nJohn', '2022-03-10', 0),
                                                                                                    (12, 5, 'Best places in Dallas', 'I want to spent vacation with my family. I have 2 little children\'s and on wife, Please suggest me what are the best places to spend vacations.', '2022-03-17', 0),
                                                                                                    (13, 9, 'First Trip to New Zealand', 'Hi,\r\n\r\nI am in New Zealand and trying to plan my 1 month holiday and looking for advice on where is best to start my exploration?\r\n\r\nNew Zealand has some amazing nature and I wanted to ask which Island is best to start my journey from-North or South island?\r\n\r\nI am into short and one-day walks as I won\'t be able to make any of the proper 3 day or more hikes.\r\n\r\nThe internet is full of guides and I found some useful info and tips on best one-day hikes such us here:\r\n-snip-\r\n\r\nI would be really grateful if anyone has any tips about that!\r\n\r\nCheers!', '2022-03-25', 0),
                                                                                                    (14, 4, 'London to CapeTown with our own caravan ? Is it possible?', 'Just trying to find out if possible to take our caravan to Africa to do a road trip? Many thanks.', '2022-03-29', 0);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping data for table forum.reacted_posts: ~14 rows (approximately)
/*!40000 ALTER TABLE `reacted_posts` DISABLE KEYS */;
INSERT INTO `reacted_posts` (`post_id`, `reaction_id`) VALUES
                                                           (4, 1),
                                                           (4, 2),
                                                           (3, 3),
                                                           (2, 10),
                                                           (4, 11),
                                                           (4, 12),
                                                           (9, 13),
                                                           (2, 14),
                                                           (7, 15),
                                                           (4, 16),
                                                           (9, 17),
                                                           (11, 18),
                                                           (6, 19),
                                                           (8, 20);
/*!40000 ALTER TABLE `reacted_posts` ENABLE KEYS */;

-- Dumping data for table forum.reactions: ~17 rows (approximately)
/*!40000 ALTER TABLE `reactions` DISABLE KEYS */;
INSERT INTO `reactions` (`reaction_id`, `user_id`, `reaction_type`) VALUES
                                                                        (1, 5, 1),
                                                                        (2, 4, 0),
                                                                        (3, 2, 1),
                                                                        (6, 2, 0),
                                                                        (7, 2, 0),
                                                                        (8, 2, 0),
                                                                        (10, 2, 0),
                                                                        (11, 3, 1),
                                                                        (12, 2, 1),
                                                                        (13, 4, 1),
                                                                        (14, 11, 0),
                                                                        (15, 5, 1),
                                                                        (16, 9, 1),
                                                                        (17, 9, 1),
                                                                        (18, 9, 1),
                                                                        (19, 9, 0),
                                                                        (20, 2, 1);
/*!40000 ALTER TABLE `reactions` ENABLE KEYS */;

-- Dumping data for table forum.tagged_posts: ~17 rows (approximately)
/*!40000 ALTER TABLE `tagged_posts` DISABLE KEYS */;
INSERT INTO `tagged_posts` (`post_id`, `tag_id`) VALUES
                                                     (3, 1),
                                                     (3, 5),
                                                     (2, 3),
                                                     (2, 2),
                                                     (6, 15),
                                                     (6, 17),
                                                     (6, 14),
                                                     (6, 16),
                                                     (9, 19),
                                                     (9, 18),
                                                     (10, 21),
                                                     (10, 20),
                                                     (11, 23),
                                                     (11, 22),
                                                     (12, 24),
                                                     (5, 3),
                                                     (5, 2);
/*!40000 ALTER TABLE `tagged_posts` ENABLE KEYS */;

-- Dumping data for table forum.tag_types: ~17 rows (approximately)
/*!40000 ALTER TABLE `tag_types` DISABLE KEYS */;
INSERT INTO `tag_types` (`tag_id`, `tag_name`) VALUES
                                                   (15, 'adventure'),
                                                   (23, 'aroundtheworld'),
                                                   (1, 'camping'),
                                                   (18, 'chicago'),
                                                   (24, 'dallas'),
                                                   (5, 'equipment'),
                                                   (19, 'firsttime'),
                                                   (2, 'hiking'),
                                                   (22, 'journey'),
                                                   (3, 'mountains'),
                                                   (13, 'newtag'),
                                                   (17, 'patagonia'),
                                                   (20, 'run'),
                                                   (16, 'season'),
                                                   (4, 'stara_planina'),
                                                   (14, 'trip'),
                                                   (21, 'you33');
/*!40000 ALTER TABLE `tag_types` ENABLE KEYS */;

-- Dumping data for table forum.users: ~11 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `username`, `password`, `is_blocked`, `is_admin`, `is_deleted`, `avatar_id`) VALUES
                                                                                                                                                     (2, 'John', 'Does', 'john.doe@gmail.com', 'John123', '12345678', 0, 1, 0, 2),
                                                                                                                                                     (3, 'Jane', 'Doe', 'jane.doe@yahoo.com', 'missDoe', '1345efds', 0, 1, 0, 2),
                                                                                                                                                     (4, 'Marks', 'Browns', 'markb@gmail.com', 'bigMark', 'pass1234', 0, 1, 0, 2),
                                                                                                                                                     (5, 'Peter', 'Parker', 'peter.parker@gmail.com', 'Peter123', '12345678', 0, 0, 0, 1),
                                                                                                                                                     (6, 'Marry', 'Jane', 'jane.marry@yahoo.com', 'missJane', '1345efds', 1, 0, 0, 3),
                                                                                                                                                     (7, 'Lilia', 'Georgieva', 'lilly@georgieff.bg', 'Lileto', '123456789', 0, 0, 0, 1),
                                                                                                                                                     (8, 'Tomm', 'Sowyer', 'Tom@world.bg', 'TravelingTom', '12345678', 0, 0, 0, 1),
                                                                                                                                                     (9, 'World', 'Traveler', 'traveler@abv.bg', 'WrldTraveler', '12345678', 0, 0, 0, 1),
                                                                                                                                                     (10, 'Jack', 'Sparrow', 'jack@balacksea.bg', 'PirateJack', '12345678', 0, 0, 0, 1),
                                                                                                                                                     (11, 'Jayson', 'Austin', 'jayson2828@gmail.com', 'Jay1000', 'pass1234', 0, 0, 0, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
