package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Tag;
import com.team11.travelforum.repositories.contracts.TagRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.team11.travelforum.Helpers.createMockTag;

@ExtendWith(MockitoExtension.class)
public class TagServiceTests {

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    TagServiceImpl tagService;

    @Test
    public void getAllTags_should_callRepository() {
        // Arrange
        Mockito.when(tagRepository.getAllTags())
                .thenReturn(new ArrayList<>());

        // Act
        tagService.getAllTags();

        // Assert
        Mockito.verify(tagRepository, Mockito.times(1))
                .getAllTags();
    }

    @Test
    public void getById_Should_ReturnTag_When_MatchExists() {
        //Arrange
        Tag mockTag = createMockTag();
        Mockito.when(tagRepository.getById(mockTag.getId())).thenReturn(mockTag);

        //Act
        Tag result = tagService.getById(mockTag.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockTag.getId(), result.getId()),
                () -> Assertions.assertEquals(mockTag.getName(), result.getName())

        );
    }

    @Test
    public void getByName_Should_Return_ByName(){
        //Arrange
        Tag mockTag = createMockTag();

        Mockito.when(tagRepository.getByName(mockTag.getName())).thenReturn(mockTag);

        //Act
        Tag result = tagService.getByName(mockTag.getName());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockTag.getId(), result.getId()),
                () -> Assertions.assertEquals(mockTag.getName(), result.getName()));

    }

    @Test
    public void create_Should_Throw_When_TagAlreadyExists(){
        //Arrange
        Tag mockTag = createMockTag();

        Mockito.when(tagRepository.getByName(Mockito.anyString())).thenReturn(mockTag);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> tagService.create(mockTag));
    }

    @Test
    public void create_should_callRepository_When_TagNotDuplicate(){
        //Arrange
        Tag mockTag = createMockTag();

        Mockito.when(tagRepository.getByName(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException("Tag", "name", mockTag.getName()));

        //Act
        tagService.create(mockTag);

        //Assert
        Mockito.verify(tagRepository, Mockito.times(1)).create(mockTag);
    }

    @Test
    public void update_Should_Throw_When_TagAlreadyExists(){
        Tag mockTag = createMockTag();
        Tag duplicateMockTag = createMockTag();
        duplicateMockTag.setId(2);

        Mockito.when(tagRepository.getByName(mockTag.getName())).thenReturn(duplicateMockTag);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> tagService.update(mockTag));
    }

    @Test
    public void update_should_callRepository_WhenTagNotDuplicate(){
        //Arrange
        Tag mockTag = createMockTag();
        Mockito.when(tagRepository.getByName(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException("Tag", "name", mockTag.getName()));

        //Act
        tagService.update(mockTag);

        //Assert
        Mockito.verify(tagRepository, Mockito.times(1)).update(mockTag);
    }

}
