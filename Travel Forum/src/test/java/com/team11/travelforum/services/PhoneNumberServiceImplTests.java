package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.PhoneNumber;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.PhoneNumberRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.team11.travelforum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PhoneNumberServiceImplTests {

    @Mock
    PhoneNumberRepository phoneNumberRepository;

    @InjectMocks
    PhoneNumberServiceImpl phoneNumberService;

    @Test
    public void getAll_Should_Throw_WhenUserIsNotAdmin(){
        //Arrange
        User mockUser = createMockUser();

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> phoneNumberService.getAll(mockUser));
    }

    @Test
    public void getAll_should_callRepository(){
        //Arrange
        User mockAdmin = createMockAdmin();
        Mockito.when(phoneNumberRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        phoneNumberService.getAll(mockAdmin);

        //Assert
        Mockito.verify(phoneNumberRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void create_Should_Throw_When_PhoneNumberAlreadyExists(){
        //Arrange
        User mockAdmin = createMockAdmin();
        PhoneNumber mockPhoneNumber = createMockPhoneNumber();

        Mockito.when(phoneNumberRepository.getByNumber(mockPhoneNumber.getPhoneNumber()))
                .thenReturn(mockPhoneNumber);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> phoneNumberService.create(mockPhoneNumber, mockAdmin));
    }

    @Test
    public void create_Should_Throw_WhenUserIsNotAdmin(){
        //Arrange
        User mockUser = createMockUser();
        PhoneNumber mockPhoneNumber = createMockPhoneNumber();

        Mockito.when(phoneNumberRepository.getByNumber(mockPhoneNumber.getPhoneNumber()))
                .thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> phoneNumberService.create(mockPhoneNumber, mockUser));
    }

    @Test
    public void create_should_callRepository(){
        //Arrange
        User mockAdmin = createMockAdmin();
        PhoneNumber mockPhoneNumber = createMockPhoneNumber();

        Mockito.when(phoneNumberRepository.getByNumber(mockPhoneNumber.getPhoneNumber()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        phoneNumberService.create(mockPhoneNumber, mockAdmin);

        //Assert
        Mockito.verify(phoneNumberRepository, Mockito.times(1))
                .create(mockPhoneNumber);
    }
}