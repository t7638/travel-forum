package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.Reaction;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.ReactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.team11.travelforum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ReactionServiceImplTests {

    @Mock
    ReactionRepository reactionRepository;

    @InjectMocks
    ReactionServiceImpl reactionService;

    @Test
    public void getAll_should_callRepository() {
        // Arrange
        Post mockPost = createMockPost(createMockUser());

        Mockito.when(reactionRepository.getAll(mockPost.getId()))
                .thenReturn(new ArrayList<>());

        // Act
        reactionService.getAll(mockPost.getId());

        // Assert
        Mockito.verify(reactionRepository, Mockito.times(1))
                .getAll(mockPost.getId());
    }

    @Test
    public void getByUser_should_callRepository() {
        // Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost(createMockUser());
        Reaction mockReaction = createMockReaction(mockUser);

        Mockito.when(reactionRepository.getByUser(mockPost.getId(), mockUser.getId()))
                .thenReturn(mockReaction);

        // Act
        reactionService.getByUser(mockPost.getId(),mockUser.getId());

        // Assert
        Mockito.verify(reactionRepository, Mockito.times(1))
                .getByUser(mockPost.getId(), mockUser.getId());
    }

    @Test
    public void create_should_callUpdate_When_SameUser_Reacts_differently(){
        //Arrange
        User mockAuthor = createMockUser();
        Post mockPost = createMockPost(mockAuthor);
        Reaction mockReaction = createMockReaction(mockAuthor);
        mockPost.getReactions().add(mockReaction);

        Mockito.when(reactionRepository.getByUser(mockPost.getId(), mockAuthor.getId()))
                .thenReturn(mockReaction);

        //Act
        reactionService.create(mockPost.getId(), false, mockAuthor);

        //Assert
        Mockito.verify(reactionRepository, Mockito.times(1)).update(mockReaction);
    }

    @Test
    public void create_should_Throw_and_callDelete_When_SameUser_Reacts_TheSameWay(){
        //Arrange
        User mockAuthor = createMockUser();
        Post mockPost = createMockPost(mockAuthor);
        Reaction mockReaction = createMockReaction(mockAuthor);
        mockPost.getReactions().add(mockReaction);

        Mockito.when(reactionRepository.getByUser(mockPost.getId(), mockAuthor.getId()))
                .thenReturn(mockReaction);

        //Act, Assert
        Assertions.assertAll(
                () -> Assertions.assertThrows(DuplicateEntityException.class,
                        ()-> reactionService.create(mockPost.getId(), true, mockAuthor)),
                () -> Mockito.verify(reactionRepository, Mockito.times(1))
                        .delete(mockPost.getId(), mockReaction.getId())
        );
    }

    @Test
    public void create_should_callRepository_When_NewUser_Reacts(){
        //Arrange
        User mockAuthor = createMockUser();
        Post mockPost = createMockPost(mockAuthor);

        Mockito.when(reactionRepository.getByUser(mockPost.getId(), mockAuthor.getId()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        reactionService.create(mockPost.getId(), false, mockAuthor);

        //Assert
        Mockito.verify(reactionRepository, Mockito.times(1))
                .create(mockPost.getId(), false, mockAuthor);
    }

    @Test
    public void update_should_callRepository(){
        //Arrange
        User mockUser = createMockUser();
        Reaction mockReaction = createMockReaction(mockUser);

        //Act
        reactionService.update(mockReaction);

        //Assert
        Mockito.verify(reactionRepository, Mockito.times(1)).update(mockReaction);
    }

    @Test
    public void delete_Should_call_Repository(){
        //Arrange
        User mockAuthor = createMockUser();
        Post mockPost = createMockPost(mockAuthor);
        Reaction mockReaction = createMockReaction(mockAuthor);
        mockPost.getReactions().add(mockReaction);

        //Act
        reactionService.delete(mockPost.getId(), mockReaction.getId());

        //Assert
        Mockito.verify(reactionRepository, Mockito.times(1))
                .delete(mockPost.getId(), mockReaction.getId());
    }

}
