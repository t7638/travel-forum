package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.team11.travelforum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getAllUsers_should_callRepository() {
        //Arrange
        Mockito.when(userRepository.getAllUsers())
                .thenReturn(new ArrayList<>());

        //Act
        userService.getAllUsers();

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .getAllUsers();
    }

    @Test
    public void getUserById_Should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getById(mockUser.getId())).thenReturn(mockUser);

        //Act
        User result = userService.getUserById(mockUser.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.isAdmin(), result.isAdmin()),
                () -> Assertions.assertEquals(mockUser.isBlocked(), result.isBlocked()),
                () -> Assertions.assertEquals(mockUser.isDeleted(), result.isDeleted())
        );
    }

    @Test
    public void getByUsername_Should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getUserByUsername(mockUser.getUsername())).thenReturn(mockUser);

        //Act
        User result = userService.getByUsername(mockUser.getUsername());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.isAdmin(), result.isAdmin()),
                () -> Assertions.assertEquals(mockUser.isBlocked(), result.isBlocked()),
                () -> Assertions.assertEquals(mockUser.isDeleted(), result.isDeleted())
        );
    }

    @Test
    public void search_should_callRepository() {
        // Arrange
        User mockUser = createMockUser();
        Optional<String> testSearch = Optional.of("testSearch");
        Mockito.when(userRepository.search(testSearch))
                .thenReturn(new ArrayList<>());

        // Act
        userService.search(testSearch, mockUser);

        // Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .search(testSearch);
    }

    @Test
    public void createUser_Should_Throw_When_UsernameAlreadyExists(){
        //Arrange
        User mockUser = createMockUser();

        Mockito.when(userRepository.getUserByUsername(Mockito.anyString())).thenReturn(mockUser);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.createUser(mockUser));
    }

    @Test
    public void createUser_Should_Throw_When_EmailAlreadyExists(){
        //Arrange
        User mockUser = createMockUser();

        Mockito.when(userRepository.getUserByUsername(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException("User", "username", mockUser.getUsername()));
        Mockito.when(userRepository.getByEmail(Mockito.anyString())).thenReturn(mockUser);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.createUser(mockUser));
    }

    @Test
    public void createUser_should_callRepository_When_UserInNotDuplicate(){
        //Arrange
        User mockUser = createMockUser();

        Mockito.when(userRepository.getUserByUsername(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException("User", "username", mockUser.getUsername()));
        Mockito.when(userRepository.getByEmail(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException("User", "email", mockUser.getEmail()));

        //Act
        userService.createUser(mockUser);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).createUser(mockUser);
    }

    @Test
    public void promoteToAdmin_Should_Throw_If_Unauthorized(){
        //Arrange
        User mockPromoter = createMockUser();
        User mockPromoted = createMockUser();
        mockPromoted.setId(mockPromoter.getId() + 1);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> userService.promoteToAdmin(mockPromoted.getId(), mockPromoter));
    }

    @Test
    public void promoteToAdmin_Should_Throw_If_Blocked(){
        //Arrange
        User mockPromoter = createMockAdmin();
        User mockPromoted = createBlockedMockUser();
        mockPromoted.setId(mockPromoter.getId() + 1);

        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(mockPromoted);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> userService.promoteToAdmin(mockPromoted.getId(), mockPromoter));
    }

//    @Test
//    public void promoteToAdmin_Should_callRepository_when_UserIsAuthorized_And_NotBlocked(){
//        //Arrange
//        User mockPromoter = createMockAdmin();
//        User mockPromoted = createMockUser();
//        mockPromoted.setId(mockPromoter.getId() + 1);
//
//        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(mockPromoted);
//
//        //Act
//        userService.promoteToAdmin(mockPromoted.getId(), mockPromoter);
//
//        //Assert
//        Mockito.verify(userRepository, Mockito.times(1)).updateUser(mockPromoted);
//    }

    @Test
    public void update_should_Throw_When_UserIsNotOwner(){
        //Arrange
        User mockUser = createMockUser();
        User mockAnotherUser = createMockUser();
        mockAnotherUser.setUsername("anotherUsername");

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.updateUser(mockUser, 2, mockAnotherUser));
    }

    @Test
    public void update_should_callRepository_When_UserIsOwner(){
        //Arrange
        User mockUser = createMockUser();

        //Act
        userService.updateUser(mockUser, 2, mockUser);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).updateUser(mockUser);
    }

    @Test
    public void blockUser_should_Throw_When_UserIsNotAdmin(){
        //Arrange
        User mockBlocker = createMockUser();
        User mockToBeBlocked = createMockUser();
        mockToBeBlocked.setId(mockBlocker.getId() + 1);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.blockUser(mockToBeBlocked.getId(), mockBlocker));
    }

    @Test
    public void blockUser_should_Throw_When_Blocking_Admin(){
        //Arrange
        User mockBlocker = createMockAdmin();
        User mockToBeBlocked = createMockAdmin();
        mockToBeBlocked.setId(mockBlocker.getId() + 1);

        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(mockToBeBlocked);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.blockUser(mockToBeBlocked.getId(), mockBlocker));
    }

    @Test
    public void blockUser_should_callRepository_When_AdminBlocksUser(){
        //Arrange
        User mockBlocker = createMockAdmin();
        User mockToBeBlocked = createMockUser();
        mockToBeBlocked.setId(mockBlocker.getId() + 1);

        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(mockToBeBlocked);

        //Act
        userService.blockUser(mockToBeBlocked.getId(), mockBlocker);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .blockUser(mockToBeBlocked.getId());
    }

    @Test
    public void delete_should_Throw_When_UserIsNotOwner(){
        //Arrange
        User mockUser = createMockUser();
        User mockAnotherUser = createMockUser();
        mockAnotherUser.setId(2);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.delete(mockUser.getId(), mockAnotherUser));
    }

    @Test
    public void delete_should_callRepository_When_UserIsOwner(){
        //Arrange
        User mockUser = createMockUser();

        //Act
        userService.delete(mockUser.getId(), mockUser);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).deleteUser(mockUser.getId());
    }
}