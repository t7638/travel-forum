package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.Comment;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.CommentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.team11.travelforum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CommentServiceImplTests {

    @Mock
    CommentRepository commentRepository;

    @InjectMocks
    CommentServiceImpl commentService;

    @Test
    public void getAllComments_should_callRepository(){
        //Arrange
        Mockito.when(commentRepository.getAllComments(Mockito.anyInt()))
                .thenReturn(new ArrayList<>());

        //Act
        commentService.getAllComments(Mockito.anyInt());

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .getAllComments(Mockito.anyInt());
    }

    @Test
    public void getCommentById_Should_ReturnComment_When_MatchExists(){
        //Arrange
        User mockUser = createMockUser();
        Comment mockComment = createMockComment(mockUser);
        Post mockPost = createMockPost(createMockUser());
        mockPost.getComments().add(mockComment);

        Mockito.when(commentRepository.getCommentById(mockPost.getId(), mockComment.getId()))
                .thenReturn(mockComment);

        //Act
        Comment result = commentService.getCommentById(mockPost.getId(), mockComment.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockComment.getId(), result.getId()),
                () -> Assertions.assertEquals(mockComment.getAuthor(), result.getAuthor()),
                () -> Assertions.assertEquals(mockComment.getContent(), result.getContent())
        );
    }

    @Test
    public void getCommentsByAuthor_Should_ReturnComments_When_MatchExists(){
        //Arrange
        User mockPostAuthor = createMockUser();
        User mockUser = createMockUser();
        Post mockPost = createMockPost(mockPostAuthor);
        Comment mockComment = createMockComment(mockUser);
        mockPost.getComments().add(mockComment);

        Mockito.when(commentRepository.getCommentsByAuthor(mockPost.getId(), mockUser.getUsername()))
                .thenReturn(List.copyOf(mockPost.getComments()));

        //Act
        List<Comment> result = commentService.getCommentsByAuthor(mockPost.getId(), mockUser.getUsername());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockPost.getComments().stream()
                                .filter(comment -> comment.getAuthor().getUsername().equals(mockUser.getUsername()))
                                .count(), result.size())
        );
    }

    @Test
    public void createComment_Should_Throw_When_AuthorIsBlocked(){
        //Arrange
        User blockedMockUser = createBlockedMockUser();
        Comment mockComment = createMockComment(blockedMockUser);

        Post mockPost = createMockPost(createMockUser());

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () ->  commentService.createComment(mockPost.getId(), mockComment, blockedMockUser));
    }

    @Test
    public void createComment_should_callRepository_When_AuthorInNotBlocked(){
        //Arrange
        User mockUser = createMockUser();
        Comment mockComment = createMockComment(mockUser);
        Post mockPost = createMockPost(createMockUser());

        //Act
        commentService.createComment(mockPost.getId(), mockComment, mockUser);

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .createComment(mockPost.getId(), mockComment);
    }

    @Test
    public void updateComment_Should_Throw_When_CommentDoesntExist(){
        //Arrange
        User mockUser = createMockUser();
        Comment mockComment = createMockComment(mockUser);
        Post mockPost = createMockPost(createMockUser());

        Mockito.when(commentRepository.getCommentById(mockPost.getId(), mockComment.getId()))
                .thenThrow(new EntityNotFoundException("Comment", mockComment.getId()));

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> commentService.updateComment(mockPost.getId(), mockComment.getId(),
                        mockComment.getContent(), mockUser));
    }

    @Test
    public void updateComment_Should_Throw_When_User_IsNot_ItsAuthor(){
        //Arrange
        User mockUser = createMockUser();
        mockUser.setId(2);
        User mockCommentAuthor = createMockUser();
        Comment mockComment = createMockComment(mockCommentAuthor);
        String newContent = "new mock content";
        Post mockPost = createMockPost(createMockUser());

        Mockito.when(commentRepository.getCommentById(mockPost.getId(), mockComment.getId()))
                .thenReturn(mockComment);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> commentService.updateComment(mockPost.getId(), mockComment.getId(),
                        newContent, mockUser));
    }

    @Test
    public void update_Should_callRepository_When_UserIsAuthor(){
        //Arrange
        User mockCommentAuthor = createMockUser();
        Comment mockComment = createMockComment(mockCommentAuthor);
        String newContent = "new mock content";
        Post mockPost = createMockPost(createMockUser());

        Mockito.when(commentRepository.getCommentById(mockPost.getId(), mockComment.getId()))
                .thenReturn(mockComment);

        //Act
        commentService.updateComment(mockPost.getId(), mockComment.getId(), newContent, mockCommentAuthor);

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .updateComment(mockPost.getId(), mockComment);
    }

    @Test
    public void deleteComment_Should_Throw_When_CommentDoesntExist(){
        //Arrange
        User mockUser = createMockUser();
        Comment mockComment = createMockComment(mockUser);
        Post mockPost = createMockPost(createMockUser());

        Mockito.when(commentRepository.getCommentById(mockPost.getId(), mockComment.getId()))
                .thenThrow(new EntityNotFoundException("Comment", mockComment.getId()));

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> commentService.deleteComment(mockPost.getId(), mockComment.getId(), mockUser));
    }

    @Test
    public void deleteComment_Should_Throw_When_UserNotAdminOrAuthor(){
        //Arrange
        User mockUser = createMockUser();
        mockUser.setId(2);
        User mockCommentAuthor = createMockUser();
        Comment mockComment = createMockComment(mockCommentAuthor);
        Post mockPost = createMockPost(createMockUser());

        Mockito.when(commentRepository.getCommentById(mockPost.getId(), mockComment.getId()))
                .thenReturn(mockComment);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> commentService.deleteComment(mockPost.getId(), mockComment.getId(), mockUser));
    }

    @Test
    public void delete_Should_callRepository_When_UserIsAuthor(){
        //Arrange
        User mockCommentAuthor = createMockUser();
        Comment mockComment = createMockComment(mockCommentAuthor);
        Post mockPost = createMockPost(createMockUser());

        Mockito.when(commentRepository.getCommentById(mockPost.getId(), mockComment.getId()))
                .thenReturn(mockComment);

        //Act
        commentService.deleteComment(mockPost.getId(), mockComment.getId(), mockCommentAuthor);

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .delete(mockPost.getId(), mockComment.getId(), mockCommentAuthor);
    }

    @Test
    public void delete_Should_callRepository_When_UserIsAdmin(){
        //Arrange
        User mockAdmin = createMockAdmin();
        mockAdmin.setId(2);
        User mockCommentAuthor = createMockUser();
        Comment mockComment = createMockComment(mockCommentAuthor);
        Post mockPost = createMockPost(createMockUser());

        Mockito.when(commentRepository.getCommentById(mockPost.getId(), mockComment.getId()))
                .thenReturn(mockComment);

        //Act
        commentService.deleteComment(mockPost.getId(), mockComment.getId(), mockAdmin);

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .delete(mockPost.getId(), mockComment.getId(), mockAdmin);
    }
}