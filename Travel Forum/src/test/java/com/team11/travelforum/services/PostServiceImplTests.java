package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.PostRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.team11.travelforum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PostServiceImplTests {

    @Mock
    PostRepository postRepository;

    @InjectMocks
    PostServiceImpl postService;

    @Test
    public void getAllPosts_should_callRepository() {
        // Arrange
        Mockito.when(postRepository.getAllPosts(Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        postService.getAllPosts(Optional.empty(),Optional.empty());

        // Assert
        Mockito.verify(postRepository, Mockito.times(1))
                .getAllPosts(Optional.empty());
    }

    @Test
    public void getAllPosts_should_callRepository_When_RandomUser() {
        // Arrange
        Mockito.when(postRepository.getAllPosts(Optional.of("comments")))
                .thenReturn(new ArrayList<>());

        // Act
        postService.getAllPosts(Optional.of("comments"),Optional.empty());

        // Assert
        Mockito.verify(postRepository, Mockito.times(1))
                .getAllPosts(Optional.of("comments"));
    }
    @Test
    public void getAllPosts_Should_Throw_When_Unauthenticated(){

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> postService.getAllPosts(Optional.of("something"),Optional.empty()));
    }


    @Test
    public void getById_Should_ReturnPost_When_MatchExists(){
        //Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost(mockUser);
        Mockito.when(postRepository.getById(mockPost.getId())).thenReturn(mockPost);

        //Act
        Post result = postService.getById(mockPost.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockPost.getId(), result.getId()),
                () -> Assertions.assertEquals(mockPost.getAuthor(), result.getAuthor()),
                () -> Assertions.assertEquals(mockPost.getContent(), result.getContent())
//                ,() -> Assertions.assertEquals(mockPost.getComments(), result.getComments())
        );
    }

    @Test
    public void filter_Should_CallRepository(){
        // Arrange
        Mockito.when(postRepository.filter(Optional.empty(),Optional.empty(),
                        Optional.empty(),Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        postService.filter(Optional.empty(),Optional.empty(),Optional.empty(),
                Optional.empty());

        // Assert
        Mockito.verify(postRepository, Mockito.times(1))
                .filter(Optional.empty(),Optional.empty(),Optional.empty(),
                        Optional.empty());

    }

    @Test
    public void create_Should_CallRepository(){
        User mockAuthor = createMockUser();
        Post mockPost = createMockPost(mockAuthor);
//        // Arrange
//        Mockito.when(postRepository.create(mockPost))
//                .thenReturn(new ArrayList<>());

        // Act
        postService.create(mockPost,mockAuthor);

        // Assert
        Mockito.verify(postRepository, Mockito.times(1))
                .create(mockPost);
    }

    @Test
    public void create_Should_Throw_When_UserIsBlock(){
        //Arrange
        User mockUser = createBlockedMockUser();
        Post mockPost = createMockPost(mockUser);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> postService.create(mockPost, mockUser));
    }

    @Test
    public void update_Should_CallRepository(){
        User mockAuthor = createMockUser();
        mockAuthor.setId(mockAuthor.getId()+1);

        Post mockPost = createMockPost(mockAuthor);

        Mockito.when(postRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);
        // Act
        postService.update(mockPost,mockAuthor);

        // Assert
        Mockito.verify(postRepository, Mockito.times(1))
                .update(mockPost);
    }

    @Test
    public void update_Should_Throw_When_UserIsNotAuthor(){
        //Arrange
        User mockAuthor = createMockUser();
        User mockRandomUser = createMockUser();
        mockAuthor.setId(mockAuthor.getId()+1);

        Post mockPost = createMockPost(mockAuthor);

        Mockito.when(postRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> postService.update(mockPost, mockRandomUser));
    }


    @Test
    public void delete_should_Throw_When_NotCreatorOrAdmin(){
        User mockCreator = createMockUser();
        Post mockPost= createMockPost(mockCreator);

        User mockRandomUser = createMockUser();

        mockCreator.setId(mockCreator.getId()+1);

        Mockito.when(postRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> postService.delete(mockPost.getId(), mockRandomUser));

    }

    // 1 more test if user is admin

    @Test
    public void delete_Should_CallRepository(){
        User mockAuthor = createMockUser();
        mockAuthor.setId(mockAuthor.getId()+1);

        Post mockPost = createMockPost(mockAuthor);

        Mockito.when(postRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);
        // Act
        postService.delete(mockPost.getId(),mockAuthor);

        // Assert
        Mockito.verify(postRepository, Mockito.times(1))
                .delete(mockPost.getId());
    }







}
