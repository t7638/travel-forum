package com.team11.travelforum;

import com.team11.travelforum.models.*;

import java.util.HashSet;

public class Helpers {
    public static User createMockUser() {
        return createMockUser(false, false);
    }

    public static User createBlockedMockUser() {
        return createMockUser(false, true);
    }

    public static User createMockAdmin() {
        return createMockUser(true, false);
    }

    private static User createMockUser(Boolean isAdmin, Boolean isBlocked) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setAdmin(isAdmin);
        mockUser.setBlocked(isBlocked);
        return mockUser;
    }

    public static Post createMockPost(User author){
            var mockPost = new Post();
            mockPost.setId(1);
            mockPost.setAuthor(author);
            mockPost.setContent("Mock content of the mock post by mock user");
            mockPost.setComments(new HashSet<>());
            mockPost.setReactions(new HashSet<>());
            mockPost.setTags(new HashSet<>());
            return mockPost;
    }

    public static Comment createMockComment(User author){
        var mockComment = new Comment();
        mockComment.setId(1);
        mockComment.setAuthor(author);
        mockComment.setContent("Mock comment content about a mock post.");
        return mockComment;
    }

    public static Tag createMockTag(){
        var mockTag = new Tag();
        mockTag.setId(1);
        mockTag.setName("Tag");
        return mockTag;
    }

    public static PhoneNumber createMockPhoneNumber(){
        var mockPhoneNumber = new PhoneNumber();
        mockPhoneNumber.setId(1);
        mockPhoneNumber.setPhoneNumber("0888888888");
        return mockPhoneNumber;
    }

    public  static Reaction createMockReaction(User user){
        var mockReaction = new Reaction();
        mockReaction.setReaction(true);
        mockReaction.setId(1);
        mockReaction.setUser(user);
        return mockReaction;
    }
}
