package com.team11.travelforum.repositories;

import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.AvatarRepository;
import com.team11.travelforum.repositories.contracts.PhoneNumberRepository;
import com.team11.travelforum.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;
    private final AvatarRepository avatarRepository;
    private final PhoneNumberRepository phoneNumberRepository;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, AvatarRepository avatarRepository, PhoneNumberRepository phoneNumberRepository) {
        this.sessionFactory = sessionFactory;
        this.avatarRepository = avatarRepository;
        this.phoneNumberRepository = phoneNumberRepository;
    }

    @Override
    public List<User> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where isDeleted = false ", User.class);
            return query.list();
        }
    }

    @Override
    public BigInteger getUsersCount() {
        try (Session session = sessionFactory.openSession()) {
            return (BigInteger) session.createNativeQuery(
                    "select count(*) from users where is_deleted = false").getSingleResult();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }

    }

    @Override
    public User getByFirstName(String firstName) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where firstName= :firstName and isDeleted = false", User.class);
            query.setParameter("firstName", firstName);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "first name", firstName);
            } else {
                return users.get(0);
            }
        }
    }

    @Override
    public User getByLastName(String lastName) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where lastName= :lastName and isDeleted=false", User.class);
            query.setParameter("lastName", lastName);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "last name", lastName);
            } else {
                return users.get(0);
            }
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username= :username and isDeleted = false", User.class);
            query.setParameter("username", username);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            } else {
                return users.get(0);
            }
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email= :email and isDeleted = false", User.class);
            query.setParameter("email", email);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            } else {
                return users.get(0);
            }
        }
    }


    @Override
    public void createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void updateUser(User user) {
        User oldUser = getById(user.getId());
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            if(oldUser.getPhoneNumber() == null && user.getPhoneNumber() != null){
                phoneNumberRepository.create(user.getPhoneNumber());
            }
            else if (user.getPhoneNumber()!=null && !oldUser.getPhoneNumber().getPhoneNumber()
                    .equals(user.getPhoneNumber().getPhoneNumber())) {
                phoneNumberRepository.delete(oldUser.getPhoneNumber().getId());
                phoneNumberRepository.create(user.getPhoneNumber());
            }
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void blockUser(int id) {
        User userToUpdate = getById(id);
        if (userToUpdate.isBlocked()) {
            userToUpdate.setBlocked(false);
            userToUpdate.setAvatar(avatarRepository.getById(1));
        } else {
            userToUpdate.setBlocked(true);
            userToUpdate.setAvatar(avatarRepository.getById(3));
        }
        updateUser(userToUpdate);
    }

    @Override
    public void deleteUser(int id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            userToDelete.setDeleted(true);
            userToDelete.setUsername("Deleted User" + id);
            updateUser(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAllUsers();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where isDeleted = false and username like :name or firstName like :name or email like :email ",
                    User.class);
            query.setParameter("name", "%" + search.get() + "%");
            query.setParameter("email", "%" + search.get() + "%");

            return query.list();
        }
    }
}
