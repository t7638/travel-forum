package com.team11.travelforum.repositories;

import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.repositories.contracts.PostRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class PostRepositoryImpl implements PostRepository {

    private static final String UNSUPPORTED_SORT =
            "Sort should have at least one or maximum two params divided by _ symbol!";

    private final SessionFactory sessionFactory;

    @Autowired
    public PostRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Post> getAllPosts(Optional<String> sort) {
        try(Session session = sessionFactory.openSession()){
            var queryString = new StringBuilder(" from Post as post ");

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            Query<Post> query = session.createQuery(queryString.toString(), Post.class);
            return query.list().stream()
                    .filter(post -> !post.isDeleted())
                    .collect(Collectors.toList());
        }
    }

    @Override
    public BigInteger getPostsCount() {
        try (Session session = sessionFactory.openSession()) {
            return (BigInteger) session.createNativeQuery(
                    "select count(*) from posts where is_deleted = false").getSingleResult();
        }
    }

    @Override
    public List<Post> filter(Optional<String> title, Optional<String> tagName,
                             Optional<String> authorUsername, Optional<String> sort) {
        try(Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(
                    " select distinct post from Post as post " +
                    " left join fetch post.tags as t ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            title.ifPresent(value -> {
                filter.add(" post.title like :title ");
                queryParams.put("title", "%" + value + "%");
            });

            tagName.ifPresent(value -> {
                filter.add(" t.name = :tagName ");
                queryParams.put("tagName", value);
            });

            authorUsername.ifPresent(value -> {
                filter.add(" post.author.username = :authorUsername ");
                queryParams.put("authorUsername", value);
            });

            if (!filter.isEmpty()){
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value.trim()));
            });

            Query<Post> queryList = session.createQuery(queryString.toString(), Post.class);
            queryList.setProperties(queryParams);

            return queryList.list().stream()
                    .filter(post -> !post.isDeleted())
                    .collect(Collectors.toList());
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty() || params.length < 1) {
            throw new UnsupportedOperationException(UNSUPPORTED_SORT);
        }

        switch (params[0]) {
            case "title":
                queryString.append(" post.title ");
                break;
            case "reactions":
                queryString.append(" size(post.reactions) ");
                break;
            case "comments":
                queryString.append(" size(post.comments) ");
                break;
            case "creationDate":
                queryString.append(" post.id ");
                break;
        }

        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException(UNSUPPORTED_SORT);
        }

        return queryString.toString();
    }

    @Override
    public Post getById(int id) {
        try (Session session = sessionFactory.openSession()){
            Post post = session.get(Post.class, id);
            if (post == null || post.isDeleted()){
                throw new EntityNotFoundException("Post", id);
            }
            return post;
        }
    }

    @Override
    public Post create(Post post) {
        try(Session session = sessionFactory.openSession()){
            session.save(post);
        }
        return post;
    }

    @Override
    public Post update(Post post) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
        }
        return post;
    }

    @Override
    public void delete(int id) {
        Post postToDel = getById(id);
        postToDel.setDeleted(true);
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(postToDel);
            session.getTransaction().commit();
        }
    }
}