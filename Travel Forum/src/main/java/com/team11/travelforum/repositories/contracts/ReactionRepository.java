package com.team11.travelforum.repositories.contracts;

import com.team11.travelforum.models.Reaction;
import com.team11.travelforum.models.User;

import java.util.List;

public interface ReactionRepository {
    List<Reaction> getAll(int postId);

    Reaction getById(int postId, int reactionId);

    Reaction getByUser(int postId, int userId);

    Reaction create(int postId, boolean reactionType, User user);

    Reaction update(Reaction reaction);

    void delete(int postId, int reactionId);
}
