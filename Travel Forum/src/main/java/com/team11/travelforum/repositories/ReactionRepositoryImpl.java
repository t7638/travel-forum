package com.team11.travelforum.repositories;

import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.Reaction;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.PostRepository;
import com.team11.travelforum.repositories.contracts.ReactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class ReactionRepositoryImpl implements ReactionRepository {

    private final SessionFactory sessionFactory;
    private final PostRepository postRepository;

    @Autowired
    public ReactionRepositoryImpl(SessionFactory sessionFactory, PostRepository postRepository) {
        this.sessionFactory = sessionFactory;
        this.postRepository = postRepository;
    }

    @Override
    public List<Reaction> getAll(int postId) {
        try (Session session = sessionFactory.openSession()){
            NativeQuery<Reaction> query = session.createNativeQuery(
                    "select r.reaction_id, user_id, reaction_type from reactions r\n" +
                            "join reacted_posts rp on r.reaction_id = rp.reaction_id\n" +
                            "where post_id = :postId");
            query.setParameter("postId", postId);
            query.addEntity(Reaction.class);
            return query.list();
        }
    }

    @Override
    public Reaction getById(int postId, int reactionId) {
        try (Session session = sessionFactory.openSession()){
            NativeQuery<Reaction> query = session.createNativeQuery(
                    "select r.reaction_id, user_id, reaction_type from reactions r\n" +
                            "join reacted_posts rp on r.reaction_id = rp.reaction_id\n" +
                            "where post_id = :postId and r.reaction_id = :reactionId");
            query.setParameter("postId", postId);
            query.setParameter("reactionId", reactionId);
            query.addEntity(Reaction.class);

            List<Reaction> reactions = query.list();
            if (reactions.isEmpty()){
                throw new EntityNotFoundException("Reaction", reactionId);
            }
            return reactions.get(0);
        }
    }

    @Override
    public Reaction getByUser(int postId, int userId) {
        try (Session session = sessionFactory.openSession()){
            NativeQuery<Reaction> query = session.createNativeQuery(
                    "select r.reaction_id, user_id, reaction_type from reactions r\n" +
                            "join reacted_posts rp on r.reaction_id = rp.reaction_id\n" +
                            "where post_id = :postId and user_id = :userId");
            query.setParameter("postId", postId);
            query.setParameter("userId", userId);
            query.addEntity(Reaction.class);

            List<Reaction> reactions = query.list();
            if (reactions.isEmpty()){
                throw new EntityNotFoundException("Reaction", "user id", String.valueOf(userId));
            }
            return reactions.get(0);
        }
    }

    @Override
    public Reaction create(int postId, boolean reactionType, User user) {
        Reaction reaction = new Reaction();
        reaction.setReaction(reactionType);
        reaction.setUser(user);

        Post postToUpdate = postRepository.getById(postId);
        Set<Reaction> updatedReactions = postToUpdate.getReactions();
        updatedReactions.add(reaction);
        postToUpdate.setReactions(updatedReactions);

        try (Session session = sessionFactory.openSession()){
            session.save(reaction);
            postRepository.update(postToUpdate);
        }
        return reaction;
    }


    @Override
    public Reaction update(Reaction reaction) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(reaction);
            session.getTransaction().commit();
        }
        return reaction;
    }

    @Override
    public void delete(int postId, int reactionId) {
        Reaction reactionToDel = getById(postId, reactionId);

        Post postToUpdate = postRepository.getById(postId);
        Set<Reaction> updatedReactions = postToUpdate.getReactions();
        updatedReactions.remove(reactionToDel);
        postToUpdate.setReactions(updatedReactions);

        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            postRepository.update(postToUpdate);
            session.delete(reactionToDel);
            session.getTransaction().commit();
        }
    }
}
