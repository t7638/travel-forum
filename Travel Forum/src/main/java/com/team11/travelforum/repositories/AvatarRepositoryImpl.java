package com.team11.travelforum.repositories;

import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Avatar;
import com.team11.travelforum.repositories.contracts.AvatarRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class AvatarRepositoryImpl implements AvatarRepository {

    private final SessionFactory sessionFactory;

    public AvatarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Avatar getById(int id) {
        try (Session session = sessionFactory.openSession()){
            Avatar avatar = session.get(Avatar.class, id);
            if (avatar == null){
                throw new EntityNotFoundException("Avatar", id);
            }
            return avatar;
        }
    }
}
