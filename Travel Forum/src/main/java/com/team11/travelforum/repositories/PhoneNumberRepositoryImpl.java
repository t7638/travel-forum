package com.team11.travelforum.repositories;

import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.PhoneNumber;
import com.team11.travelforum.repositories.contracts.PhoneNumberRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhoneNumberRepositoryImpl implements PhoneNumberRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PhoneNumberRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PhoneNumber> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<PhoneNumber> query = session.createQuery("from PhoneNumber",PhoneNumber.class);
            return query.list();
        }
    }

    @Override
    public PhoneNumber getByNumber(String phoneNumber) {
        try (Session session = sessionFactory.openSession()) {
            Query<PhoneNumber> query = session.createQuery("from PhoneNumber where phoneNumber = :phoneNumber", PhoneNumber.class);
            query.setParameter("phoneNumber", phoneNumber);
            List<PhoneNumber> phoneNumbers = query.list();
            if (phoneNumbers.size() == 0) {
                throw new EntityNotFoundException("User", "phone number", phoneNumber);
            } else {
                return phoneNumbers.get(0);
            }
        }    }

    @Override
    public PhoneNumber getById(int id) {
        try (Session session = sessionFactory.openSession()){
            PhoneNumber phoneNumber = session.get(PhoneNumber.class, id);
            if (phoneNumber == null){
                throw new EntityNotFoundException("Phone number", id);
            }
            return phoneNumber;
        }
    }

    @Override
    public PhoneNumber create(PhoneNumber phoneNumber) {
        try (Session session = sessionFactory.openSession()) {
            session.save(phoneNumber);
        }
        return phoneNumber;
    }

    @Override
    public void delete(int id) {
        PhoneNumber phoneNumberToDel = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(phoneNumberToDel);
            session.getTransaction().commit();
        }
    }
}
