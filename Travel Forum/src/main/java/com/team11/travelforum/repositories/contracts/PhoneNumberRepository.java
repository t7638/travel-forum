package com.team11.travelforum.repositories.contracts;

import com.team11.travelforum.models.PhoneNumber;

import java.util.List;

public interface PhoneNumberRepository {
    List<PhoneNumber> getAll();

    PhoneNumber getByNumber(String phoneNumber);

    PhoneNumber getById(int id);

    PhoneNumber create(PhoneNumber phoneNumber);

    void delete(int id);
}
