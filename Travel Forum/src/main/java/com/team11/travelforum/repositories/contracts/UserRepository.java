package com.team11.travelforum.repositories.contracts;

import com.team11.travelforum.models.User;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface UserRepository {

    List<User> getAllUsers();

    BigInteger getUsersCount();

    User getById(int id);

    User getByFirstName(String firstName);

    User getByLastName(String lastName);

    User getUserByUsername(String username);

    User getByEmail(String email);

    void createUser(User user);

    void updateUser(User user);

    void blockUser(int id);

    void deleteUser(int id);

    List<User> search(Optional<String> search);
}
