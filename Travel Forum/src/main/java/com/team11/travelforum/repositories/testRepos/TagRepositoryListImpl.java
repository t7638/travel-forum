package com.team11.travelforum.repositories.testRepos;

import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Tag;
import com.team11.travelforum.repositories.contracts.TagRepository;

import java.util.ArrayList;
import java.util.List;

//@Repository
public class TagRepositoryListImpl implements TagRepository {

    private final List<Tag> tags;

    public TagRepositoryListImpl() {
        tags = new ArrayList<>();

        tags.add(new Tag(1,"hiking"));
        tags.add(new Tag(2,"mountains"));
        tags.add(new Tag(3,"stara_planina"));
        tags.add(new Tag(4,"equipment"));
    }

    @Override
    public List<Tag> getAllTags(){
        return tags;
    }

    @Override
    public Tag getById(int id) {
        return tags.stream()
                .filter(tag -> tag.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Tag", id));
    }

    @Override
    public Tag getByName(String name){
        return tags.stream()
                .filter(tag -> tag.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Tag", "name", name));
    }

    @Override
    public void create(Tag tag){
        tags.add(tag);
    }

    @Override
    public void update(Tag tag){
        Tag tagToUpdate = getById(tag.getId());

        tagToUpdate.setName(tag.getName());
    }
}
