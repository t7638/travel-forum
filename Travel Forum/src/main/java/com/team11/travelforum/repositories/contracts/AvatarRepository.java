package com.team11.travelforum.repositories.contracts;

import com.team11.travelforum.models.Avatar;

public interface AvatarRepository {
    Avatar getById(int id);
}
