package com.team11.travelforum.repositories.contracts;

import com.team11.travelforum.models.Comment;
import com.team11.travelforum.models.User;

import java.util.List;

public interface CommentRepository {
    List<Comment> getAllComments(int postId);

    Comment getCommentById(int postId,int id);

    List<Comment> getCommentsByAuthor(int postId, String username);

    void createComment(int postId, Comment comment);

    void updateComment(int postId, Comment comment);

    void delete(int postId, int id, User user);
}
