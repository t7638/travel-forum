package com.team11.travelforum.repositories;

import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Comment;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.Reaction;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.CommentRepository;
import com.team11.travelforum.repositories.contracts.PostRepository;
import com.team11.travelforum.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class CommentRepositoryImpl implements CommentRepository {

    private final SessionFactory sessionFactory;
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public CommentRepositoryImpl(SessionFactory sessionFactory, PostRepository postRepository, UserRepository userRepository) {
        this.sessionFactory = sessionFactory;
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Comment> getAllComments(int postId) {

        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Comment> query = session.createNativeQuery(
                    "select c.comment_id, author_id, content, creation_date from comments c\n" +
                            "join commented_posts cp on c.comment_id = cp.comment_id\n" +
                            "where post_id = :postId");
            query.setParameter("postId", postId);
            query.addEntity(Comment.class);
            List<Comment> comments = query.list();
            if (comments.isEmpty()) {
                throw new EntityNotFoundException("Comments", "post", String.valueOf(postId));
            }
            return comments;
        }
    }

    @Override
    public Comment getCommentById(int postId, int id) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Comment> query = session.createNativeQuery(
                    "select c.comment_id, author_id, content, creation_date from comments c\n" +
                            "join commented_posts cp on c.comment_id = cp.comment_id\n" +
                            "where post_id = :postId and c.comment_id = :reactionId");
            query.setParameter("postId", postId);
            query.setParameter("reactionId", id);
            query.addEntity(Comment.class);

            List<Comment> comments = query.list();
            if (comments.isEmpty()) {
                throw new EntityNotFoundException("Comment", id);
            }
            return comments.get(0);
        }
    }

        @Override
        public List<Comment> getCommentsByAuthor (int postId, String username){
            try (Session session = sessionFactory.openSession()){
                NativeQuery<Comment> query = session.createNativeQuery(
                        "select c.comment_id, author_id, content, creation_date from comments c\n" +
                                "join commented_posts cp on c.comment_id = cp.comment_id\n" +
                                "where post_id = :postId and author_id = :userId");
                query.setParameter("postId", postId);
                query.setParameter("userId", userRepository.getUserByUsername(username).getId());
                query.addEntity(Reaction.class);

                List<Comment> comments = query.list();
                if (comments.isEmpty()){
                    throw new EntityNotFoundException("Comment", "author", username);
                }
                return comments;
            }
        }

        @Override
        public void createComment (int postId, Comment comment){
            Post postToUpdate = postRepository.getById(postId);
            Set<Comment> updatedComments = postToUpdate.getComments();
            updatedComments.add(comment);
            postToUpdate.setComments(updatedComments);

            try (Session session = sessionFactory.openSession()){
                session.save(comment);
                postRepository.update(postToUpdate);
            }
        }

        @Override
        public void updateComment (int postId, Comment comment){
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.update(comment);
                session.getTransaction().commit();
            }
        }

        @Override
        public void delete (int postId, int id, User user){
            Comment comment = getCommentById(postId, id);

            String deletedContent;
            if (comment.getAuthor().getId() == user.getId()){
                deletedContent = "Deleted by author.";
            }else {
                deletedContent = "Deleted by admin.";
            }

            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                comment.setContent(deletedContent);
                updateComment(postId, comment);
                session.getTransaction().commit();
            }
        }
    }
