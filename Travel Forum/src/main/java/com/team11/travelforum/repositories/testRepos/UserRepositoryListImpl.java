//package com.team11.travelforum.repositories.testRepos;
//
//import com.team11.travelforum.exceptions.EntityNotFoundException;
//import com.team11.travelforum.models.User;
//import com.team11.travelforum.repositories.contracts.UserRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
////@Repository
//public class UserRepositoryListImpl implements UserRepository {
//    private final List<User> users;
//
//    public UserRepositoryListImpl() {
//        users = new ArrayList<>();
//        users.add(new User(1, "John", "Doe", "john.doe@gmail.com",
//                "John123","123456", false, false));
//        users.add(new User(2, "Jane", "Doe", "jane.doe@yahoo.com",
//                "missDoe","134efds", false, true));
//        users.add(new User(3, "Mark", "Brown", "markb@gmail.com",
//                "bigMark","098asdd", true, false));
//        users.add(new User(4, "Peter", "Parker", "peter.parker@gmail.com",
//                "Peter123", "123456", false, false));
//        users.add(new User(5, "Marry", "Jane", "jane.marry@yahoo.com",
//                "missJane", "134efds", true, false));
//
//    }
//
//    @Override
//    public List<User> getUsers() {
//        return users;
//    }
//
//    @Override
//    public User getById(int id) {
//        return users.stream()
//                .filter(user -> user.getId() == id)
//                .findFirst()
//                .orElseThrow(() -> new EntityNotFoundException("User", id));
//    }
//
//    @Override
//    public User getByFirstName(String firstName) {
//        return users.stream()
//                .filter(user -> user.getFirstName().equals(firstName))
//                .findFirst()
//                .orElseThrow(() -> new EntityNotFoundException("User", "first name", firstName));
//    }
//
//    @Override
//    public User getByLastName(String lastName) {
//        return users.stream()
//                .filter(user -> user.getLastName().equals(lastName))
//                .findFirst()
//                .orElseThrow(() -> new EntityNotFoundException("User", "last name", lastName));
//    }
//
////    @Override
////    public User getByUsername(String username) {
////        return users.stream()
////                .filter(user -> user.getUsername().equals(username))
////                .findFirst()
////                .orElseThrow(() -> new EntityNotFoundException("User", "username", username));
////    }
//
//    @Override
//    public User getByEmail(String email) {
//        return null;
//    }
//
//    @Override
//    public void createUser(User user) {
//        users.add(user);
//    }
//
//    @Override
//    public void updateUser(User user) {
//        User userToUpdate = getById(user.getId());
//        userToUpdate.setFirstName(user.getFirstName());
//        userToUpdate.setLastName(user.getLastName());
//        userToUpdate.setPass(user.getPass());
//    }
//
////    @Override
////    public void makeAdmin(int id) {
////        User userToUpdate = getById(id);
////        userToUpdate.setAdmin(true);
////    }
////
//    @Override
//    public void blockUser(int id) {
//        User userToUpdate = getById(id);
//        if (userToUpdate.isBlock())
//            userToUpdate.setBlock(false);
//        else
//            userToUpdate.setBlock(true);
//
//    }
//
//    @Override
//    public void deleteUser(int id) {
//        User userToDelete = getById(id);
//        users.remove(userToDelete);
//    }
//
//    @Override
//    public List<User> search(Optional<String> search) {
//        return null;
//    }
//}
//
