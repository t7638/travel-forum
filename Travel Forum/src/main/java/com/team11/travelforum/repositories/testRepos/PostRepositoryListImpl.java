/*
package com.team11.travelforum.repositories.testRepos;

import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Comment;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.PostRepository;
import com.team11.travelforum.repositories.contracts.UserRepository;

import java.time.LocalDate;
import java.util.*;

//@Repository
public class PostRepositoryListImpl implements PostRepository {
    private final List<Post> posts;

    public PostRepositoryListImpl(UserRepository userRepository, List<Post> posts) {
        this.posts = posts;
        User user1 = userRepository.getById(4);
        User user2 = userRepository.getById(5);

        List<Comment> comments = new ArrayList<>();
        comments.add(new Comment(1, LocalDate.now(), user2, "What kind of equipment do is required?"));
        comments.add(new Comment(2, LocalDate.now(), user1, "Nothing special"));

        Map<Integer, Boolean> reactions1 = new HashMap<>();
        reactions1.put(1, true);
        reactions1.put(2, false);

        Map<Integer, Boolean> reactions2 = new HashMap<>();
        reactions2.put(1, true);
        reactions2.put(2, true);

//        posts = new ArrayList<>();
//        posts.add(new Post(1, user1, "Hiking in Stara Planina",
//                "The trail is 650-700 km long and usually it could be done for 20-25 days in the summer. " +
//                        "The hiking trails are on the main ridge of the mountain and hikers can choose whether " +
//                        "to climb more than 100 peaks along the way or go around them.",
//                comments, reactions1, new HashSet<>(), LocalDate.now()));
//
//        posts.add(new Post(2, user2, "Equipment for hiking",
//                "The list of essential hiking gear is as follows:" +
//                        "Map, Compass, Sunglasses, sun cream and a sun hat, " +
//                        "Spare warm clothing, Headlamp and/or handheld torch, First-aid kit, " +
//                        "Firestarter, Matches, Knife, More food and water than you need",
//                new ArrayList<>(), reactions2, new HashSet<>(), LocalDate.now()));
    }

    @Override
    public List<Post> getAllPosts(){
        return posts;
    }

    @Override
    public Post getById(int id){
        return posts.stream()
                .filter(post -> post.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Post", id));
    }

    @Override
    public Post create(Post post){
        posts.add(post);
        return post;
    }

    @Override
    public Post update(Post post){
        Post postToBeUpdated = getById(post.getId());

        postToBeUpdated.setTitle(post.getTitle());
        postToBeUpdated.setContent(post.getContent());
        postToBeUpdated.setTags(post.getTags());

        return postToBeUpdated;
    }

    @Override
    public void delete(int id){
        Post postToDel = getById(id);
        posts.remove(postToDel);
    }
}
*/
