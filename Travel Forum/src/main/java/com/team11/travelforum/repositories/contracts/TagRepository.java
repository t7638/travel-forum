package com.team11.travelforum.repositories.contracts;

import com.team11.travelforum.models.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getAllTags();

    Tag getById(int id);

    Tag getByName(String name);

    void create(Tag tag);

    void update(Tag tag);
}
