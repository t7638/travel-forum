package com.team11.travelforum.repositories.contracts;

import com.team11.travelforum.models.Post;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface PostRepository {
    List<Post> getAllPosts(Optional<String> sort);

    BigInteger getPostsCount();

    List<Post> filter(Optional<String> title, Optional<String> tagName,
                      Optional<String> authorUsername, Optional<String> sort);

    Post getById(int id);

    Post create(Post post);

    Post update(Post post);

    void delete(int id);
}
