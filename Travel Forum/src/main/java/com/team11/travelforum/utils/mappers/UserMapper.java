package com.team11.travelforum.utils.mappers;

import com.team11.travelforum.models.PhoneNumber;
import com.team11.travelforum.models.User;
import com.team11.travelforum.models.dtos.*;
import com.team11.travelforum.repositories.contracts.AvatarRepository;
import com.team11.travelforum.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final UserRepository userRepository;
    private final AvatarRepository avatarRepository;

    @Autowired
    public UserMapper(UserRepository userRepository, AvatarRepository avatarRepository) {
        this.userRepository = userRepository;
        this.avatarRepository = avatarRepository;
    }

    public User dtoToObject(UserDTOIn userDTOIn) {
        User user = new User();
        user.setFirstName(userDTOIn.getFirstName());
        user.setLastName(userDTOIn.getLastName());
        user.setUsername(userDTOIn.getUsername());
        user.setPassword(userDTOIn.getPassword());
        user.setEmail(userDTOIn.getEmail());
        user.setAdmin(false);
        user.setBlocked(false);
        user.setDeleted(false);
        user.setAvatar(avatarRepository.getById(1));
        return user;
    }
    public User dtoToObject(UpdateUserDTO updateUserDTO) {
        User user = userRepository.getById(updateUserDTO.getId());

        user.setFirstName(updateUserDTO.getFirstName());

        user.setLastName(updateUserDTO.getLastName());

//        user.setPassword(updateUserDTO.getPassword());

        if(user.isAdmin()){
            user.setPhoneNumber(new PhoneNumber(updateUserDTO.getPhoneNumber(), user));
        }

        return user;
    }

    public User dtoToObject(UpdatePasswordDto updatePasswordDto) {
        User user = userRepository.getById(updatePasswordDto.getId());

        user.setPassword(updatePasswordDto.getPassword());

        return user;
    }

    public UserDTOOutForAdmins userToUserDtoForAdmins(User user) {
        UserDTOOutForAdmins userDTOOutForAdmins = new UserDTOOutForAdmins();
        userDTOOutForAdmins.setId(user.getId());
        userDTOOutForAdmins.setFirstName(user.getFirstName());
        userDTOOutForAdmins.setLastName(user.getLastName());
        userDTOOutForAdmins.setUsername(user.getUsername());
        userDTOOutForAdmins.setEmail(user.getEmail());
        userDTOOutForAdmins.setAvatar(user.getAvatar());

        PhoneNumber phoneNumber = user.getPhoneNumber();
        if (phoneNumber != null) {
            userDTOOutForAdmins.setPhoneNumber(phoneNumber.getPhoneNumber());
        }
        userDTOOutForAdmins.setAdmin(user.isAdmin());
        userDTOOutForAdmins.setBlock(user.isBlocked());
        userDTOOutForAdmins.setDeleted(user.isDeleted());
        return userDTOOutForAdmins;
    }

    public UpdateUserDTO objectToUpdateDto(User user){
        UpdateUserDTO updateUserDTO = new UpdateUserDTO();
        updateUserDTO.setId(user.getId());
        updateUserDTO.setFirstName(user.getFirstName());
        updateUserDTO.setLastName(user.getLastName());
//        updateUserDTO.setPassword(user.getPassword());
//        updateUserDTO.setPasswordConfirm(user.getPassword());
        if (user.isAdmin() && user.getPhoneNumber() != null){
            updateUserDTO.setPhoneNumber(user.getPhoneNumber().getPhoneNumber());
        }

        return updateUserDTO;
    }
    public UpdatePasswordDto objectToPasswordDto(User user){
        UpdatePasswordDto updatePasswordDto = new UpdatePasswordDto();
        updatePasswordDto.setId(user.getId());
        updatePasswordDto.setPassword(user.getPassword());
        updatePasswordDto.setPasswordConfirm(user.getPassword());

        return updatePasswordDto;
    }
}


