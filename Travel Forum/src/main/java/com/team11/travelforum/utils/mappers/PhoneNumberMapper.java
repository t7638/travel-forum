package com.team11.travelforum.utils.mappers;

import com.team11.travelforum.models.PhoneNumber;
import com.team11.travelforum.models.dtos.PhoneNumberDTO;
import com.team11.travelforum.models.dtos.PhoneNumberDTOOut;
import com.team11.travelforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PhoneNumberMapper {

    private final UserService userService;

    @Autowired
    public PhoneNumberMapper(UserService userService) {
        this.userService = userService;
    }

    public PhoneNumber dtoToObject(PhoneNumberDTO phoneNumberDTO) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhoneNumber(phoneNumberDTO.getPhoneNumber());
        phoneNumber.setUser(userService.getUserById(phoneNumberDTO.getUserId()));
        return phoneNumber;
    }

    public PhoneNumberDTOOut objectToDto(PhoneNumber phoneNumber) {
        PhoneNumberDTOOut phoneNumberDTOOut = new PhoneNumberDTOOut();
        phoneNumberDTOOut.setUsername(phoneNumber.getUser().getUsername());
        phoneNumberDTOOut.setFirstName(phoneNumber.getUser().getFirstName());
        phoneNumberDTOOut.setLastName(phoneNumber.getUser().getLastName());
        phoneNumberDTOOut.setNumber(phoneNumber.getPhoneNumber());

        return phoneNumberDTOOut;
    }
}
