package com.team11.travelforum.utils.mappers;

import com.team11.travelforum.models.Comment;
import com.team11.travelforum.models.User;
import com.team11.travelforum.models.dtos.CommentDTO;
import com.team11.travelforum.models.dtos.CommentDTOOut;
import com.team11.travelforum.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class CommentMapper {

    private final UserRepository userRepository;

    @Autowired
    public CommentMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Comment dtoToObject(CommentDTO commentDTO, User user){
        Comment comment = new Comment();
        comment.setAuthor(user);
        comment.setCreationDate(LocalDate.now());
        comment.setContent(commentDTO.getContent());

        return comment;
    }

    public CommentDTO forUpdate(Comment comment){
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setContent(comment.getContent());
        return commentDTO;
    }

    public CommentDTOOut objectToDto(Comment comment) {
        CommentDTOOut commentDTOOut = new CommentDTOOut();
        commentDTOOut.setComment_id(comment.getId());
        commentDTOOut.setDate(comment.getCreationDate());
        commentDTOOut.setAuthor(comment.getAuthor().getUsername());
        commentDTOOut.setContent(comment.getContent());
        commentDTOOut.setAvatarUrl(comment.getAuthor().getAvatar().getUrl());
        return commentDTOOut;
    }
}
