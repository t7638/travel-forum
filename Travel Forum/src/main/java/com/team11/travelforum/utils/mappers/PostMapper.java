package com.team11.travelforum.utils.mappers;

import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.*;
import com.team11.travelforum.models.dtos.*;
import com.team11.travelforum.repositories.contracts.PostRepository;
import com.team11.travelforum.repositories.contracts.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PostMapper {

    private final TagRepository tagRepository;
    private final PostRepository postRepository;
    private final CommentMapper commentMapper;

    @Autowired
    public PostMapper(TagRepository tagRepository, PostRepository postRepository, CommentMapper commentMapper) {
        this.tagRepository = tagRepository;
        this.postRepository = postRepository;
        this.commentMapper = commentMapper;
    }

    public Post dtoToObject(CreatePostDTO createPostDTO, User author){
        Post post = new Post();

        post.setAuthor(author);

        post.setTitle(createPostDTO.getTitle());

        post.setContent(createPostDTO.getContent());

        post.setComments(new HashSet<>());

        post.setReactions(new HashSet<>());

        post.setTags(new HashSet<>());

        post.setCreationDate(LocalDate.now());

        post.setDeleted(false);

        return post;
    }

    public Post dtoToObject(UpdatePostDTO updatePostDTO){
        Post post = postRepository.getById(updatePostDTO.getId());

        post.setTitle(updatePostDTO.getTitle());

        post.setContent(updatePostDTO.getContent());

        if (updatePostDTO.getTagNames()
                .stream()
                .anyMatch(tag -> !tag.matches("[a-z0-9]+") || tag.length() > 15 || tag.length() < 2)){
            throw new IllegalArgumentException("Tags should be between 2 and 15 symbols (numbers or lowercase letters)!");
        }

        post.setTags(listTagsHelper(updatePostDTO.getTagNames()));

        return post;
    }

    public PostOutDTO objectToDto(Post post){
        PostOutDTO postOutDTO = new PostOutDTO();

        postOutDTO.setId(post.getId());

        postOutDTO.setAuthorId(post.getAuthor().getId());

        postOutDTO.setAuthorUsername(post.getAuthor().getUsername());

        postOutDTO.setTitle(post.getTitle());

        postOutDTO.setContent(post.getContent());

        List<CommentDTOOut> commentOutDTOS = post.getComments()
                .stream()
                .sorted(Comparator.comparing(Comment::getId))
                .map(commentMapper::objectToDto)
                .collect(Collectors.toList());
        postOutDTO.setCommentsOutDTOs(commentOutDTOS);

        long likes = post.getReactions().stream()
                .filter(Reaction::getReaction)
                .count();
        postOutDTO.setLikes(likes);

        long dislikes = post.getReactions().stream()
                .filter(reaction -> !reaction.getReaction())
                .count();
        postOutDTO.setDislikes(dislikes);

        List<String> tagNames = post.getTags()
                .stream()
                .map(Tag::getName)
                .collect(Collectors.toList());
        postOutDTO.setTagNames(tagNames);

        postOutDTO.setCreationDate(post.getCreationDate());

        return postOutDTO;
    }

    public UpdatePostDTO objectToUpdateDto(Post post){
        UpdatePostDTO updatePostDTO = new UpdatePostDTO();
        updatePostDTO.setId(post.getId());
        updatePostDTO.setTitle(post.getTitle());
        updatePostDTO.setContent(post.getContent());
        List<String> tagNames = post.getTags()
                .stream()
                .map(Tag::getName)
                .collect(Collectors.toList());
        updatePostDTO.setTagNames(tagNames);

        return updatePostDTO;
    }

    public ReactionDTO objectToDto(Reaction reaction){
        ReactionDTO reactionDTO = new ReactionDTO();

        reactionDTO.setUsername(reaction.getUser().getUsername());
        reactionDTO.setReaction(reaction.getReaction());

        return reactionDTO;
    }

    private Set<Tag> listTagsHelper(List<String> names){
        Set<Tag> tags = new HashSet<>();
        for (String name : names) {
            Tag currTag;
            try {
                currTag = tagRepository.getByName(name);
            }catch (EntityNotFoundException e){
                currTag = new Tag();
                currTag.setName(name);
                tagRepository.create(currTag);
            }

            tags.add(currTag);
        }

        return tags;
    }
}
