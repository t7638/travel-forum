package com.team11.travelforum.services.contracts;

import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.User;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface PostService {
    List<Post> getAllPosts(Optional<String> sort, Optional<User> user);

    BigInteger getPostsCount();

    Post getById(int id);

    List<Post> filter(Optional<String> title, Optional<String> tagName,
                      Optional<String> authorUsername, Optional<String> sort);

    Post create(Post post, User user);

    Post update(Post post, User user);

    void delete(int id, User user);
}
