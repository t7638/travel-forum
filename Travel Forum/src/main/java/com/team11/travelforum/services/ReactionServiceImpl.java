package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Reaction;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.ReactionRepository;
import com.team11.travelforum.services.contracts.ReactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReactionServiceImpl implements ReactionService {

    private static final String UNREACTED = "%s unreacted post with id %d";

    private final ReactionRepository reactionRepository;

    @Autowired
    public ReactionServiceImpl(ReactionRepository reactionRepository) {
        this.reactionRepository = reactionRepository;
    }

    @Override
    public List<Reaction> getAll(int postId) {
        return reactionRepository.getAll(postId);
    }

    @Override
    public Reaction getByUser(int postId, int userId) {
        return reactionRepository.getByUser(postId, userId);
    }

    @Override
    public Reaction create(int postId, boolean reactionType, User user) {
        Reaction existingReaction;
        boolean reactionExists = true;

        try {
            existingReaction = getByUser(postId, user.getId());
        }catch (EntityNotFoundException e){
            reactionExists = false;
        }

        if (reactionExists){
            existingReaction = getByUser(postId, user.getId());
            if (existingReaction.getReaction() == reactionType){
                delete(postId, existingReaction.getId());
                throw new DuplicateEntityException(
                        String.format(UNREACTED, user.getUsername(), postId));
            }
            else {
                existingReaction.setReaction(reactionType);
                return update(existingReaction);
            }
        }
        else {
           return reactionRepository.create(postId, reactionType, user);
        }

    }

    @Override
    public Reaction update(Reaction reaction) {
        return reactionRepository.update(reaction);
    }

    @Override
    public void delete(int postId, int reactionId) {
        reactionRepository.delete(postId, reactionId);
    }
}
