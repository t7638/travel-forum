package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.AvatarRepository;
import com.team11.travelforum.repositories.contracts.UserRepository;
import com.team11.travelforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final String UNAUTHORIZED_UPDATE = "Only the owner of the account can make changes!";
    private static final String UNAUTHORIZED_PROMOTION = "Only admins can promote to admin!";
    private static final String INVALID_PROMOTION = "Blocked users can't be promoted!";
    private static final String UNAUTHORIZED_BLOCK = "Only admins can block/unblock users!";
    private static final String INVALID_BLOCK = "Admins can't be blocked!";
    private static final String UNAUTHORIZED_DELETE = "Only the owner of the account can delete it!";

    private final UserRepository userRepository;
    private final AvatarRepository avatarRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, AvatarRepository avatarRepository) {
        this.userRepository = userRepository;
        this.avatarRepository = avatarRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @Override
    public BigInteger getUsersCount() {
        return userRepository.getUsersCount();
    }

    @Override
    public User getUserById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public List<User> search(Optional<String> search, User user) {
        return userRepository.search(search);
    }

    @Override
    public void createUser(User user) {
        boolean duplicateUser = true;

        try {
            userRepository.getUserByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateUser = false;
        }
        if (duplicateUser) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        duplicateUser = true;

        try {
            userRepository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateUser = false;
        }
        if (duplicateUser) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        userRepository.createUser(user);
    }

    public void promoteToAdmin(int id, User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_PROMOTION);
        }

        if (userRepository.getById(id).isBlocked()) {
            throw new UnauthorizedOperationException(INVALID_PROMOTION);
        }

        User userToPromote = userRepository.getById(id);
        userToPromote.setAdmin(true);
        userToPromote.setAvatar(avatarRepository.getById(2));
        userRepository.updateUser(userToPromote);
    }

    @Override
    public void updateUser(User user, int id, User userToUpdate) {
        if (!user.getUsername().equals(userToUpdate.getUsername())) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_UPDATE);
        }
        userRepository.updateUser(userToUpdate);
    }

    @Override
    public void blockUser(int id, User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_BLOCK);
        }
        if (userRepository.getById(id).isAdmin()) {
            throw new UnauthorizedOperationException(INVALID_BLOCK);
        }
        userRepository.blockUser(id);
    }

    @Override
    public void delete(int id, User user) {
        if (user.getId() != id) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_DELETE);
        }
        userRepository.deleteUser(id);
    }

}
