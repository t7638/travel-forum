package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Tag;
import com.team11.travelforum.repositories.contracts.TagRepository;
import com.team11.travelforum.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> getAllTags(){
        return tagRepository.getAllTags();
    }

    @Override
    public Tag getById(int id){
        return tagRepository.getById(id);
    }

    @Override
    public Tag getByName(String name) {
        return tagRepository.getByName(name);
    }

    @Override
    public void create(Tag tag){
        boolean duplicateExists = true;

        try {
            tagRepository.getByName(tag.getName());
        }
        catch (EntityNotFoundException e){
            duplicateExists = false;
        }

        if (duplicateExists){
            throw new DuplicateEntityException("Tag", "name", tag.getName());
        }

        tagRepository.create(tag);
    }

    @Override
    public void update(Tag tag){

        boolean duplicateExists = true;

        try {
            Tag existingTag = tagRepository.getByName(tag.getName());
            if (existingTag.getId() == tag.getId()){
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e){
            duplicateExists = false;
        }

        if (duplicateExists){
            throw new DuplicateEntityException("Tag", "name", tag.getName());
        }

        tagRepository.update(tag);
    }
}