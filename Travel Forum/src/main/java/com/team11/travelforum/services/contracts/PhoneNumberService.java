package com.team11.travelforum.services.contracts;

import com.team11.travelforum.models.PhoneNumber;
import com.team11.travelforum.models.User;

import java.util.List;

public interface PhoneNumberService {
    List<PhoneNumber> getAll(User user);

    PhoneNumber create(PhoneNumber phoneNumber, User userToPromote);

    void delete(int id);
}
