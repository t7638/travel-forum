package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.PhoneNumber;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.PhoneNumberRepository;
import com.team11.travelforum.services.contracts.PhoneNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhoneNumberServiceImpl implements PhoneNumberService {

    private static final String UNAUTHORIZED_GET = "Only admins can ask for phone numbers!";
    private static final String INVALID_CREATE = "Only admins can have phone number!";

    private final PhoneNumberRepository phoneNumberRepository;

    @Autowired
    public PhoneNumberServiceImpl(PhoneNumberRepository phoneNumberRepository) {
        this.phoneNumberRepository = phoneNumberRepository;
    }

    @Override
    public List<PhoneNumber> getAll(User user) {
        if(!user.isAdmin()){
            throw new UnauthorizedOperationException(UNAUTHORIZED_GET);
        }
        return phoneNumberRepository.getAll();
    }

    @Override
    public PhoneNumber create(PhoneNumber phoneNumber, User userToPromote) {
        boolean duplicatePhoneNumber = true;

        try {
            phoneNumberRepository.getByNumber(phoneNumber.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            duplicatePhoneNumber = false;
        }

        if (duplicatePhoneNumber) {
            throw new DuplicateEntityException("User", "phone number", phoneNumber.getPhoneNumber());
        }

        if(!userToPromote.isAdmin()){
            throw new UnauthorizedOperationException(INVALID_CREATE);
        }
        return phoneNumberRepository.create(phoneNumber);
    }

    @Override
    public void delete(int id) {
        phoneNumberRepository.delete(id);
    }

}
