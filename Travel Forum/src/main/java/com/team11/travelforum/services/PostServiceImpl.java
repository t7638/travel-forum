package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.PostRepository;
import com.team11.travelforum.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    private static final String UNAUTHORIZED_SORT =
            "Unauthenticated users can only sort by creation date and comments count!";
    private static final String BLOCKED_CREATION = "Blocked users can't create posts!";
    private static final String UNAUTHORIZED_UPDATE = "A post can be modified only by its author or an admin!";
    private static final String UNAUTHORIZED_DELETE = "A post can be deleted only by its author or an admin!";

    private final PostRepository postRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public List<Post> getAllPosts(Optional<String> sort, Optional<User> user){
        if (user.isEmpty() && sort.isPresent()){
            String[] sortParams = sort.get().split("_");
            if (sortParams[0].equals("comments") || sortParams[0].equals("creationDate")){
                return postRepository.getAllPosts(sort).stream()
                        .limit(10)
                        .collect(Collectors.toList());
            }
            else {
                throw new UnauthorizedOperationException(UNAUTHORIZED_SORT);
            }
        }
        else {
            return postRepository.getAllPosts(sort);
        }
    }

    @Override
    public BigInteger getPostsCount() {
        return postRepository.getPostsCount();
    }

    @Override
    public Post getById(int id){
        return postRepository.getById(id);
    }

    @Override
    public List<Post> filter(Optional<String> title, Optional<String> tagName,
                             Optional<String> authorUsername, Optional<String> sort) {
        return postRepository.filter(title, tagName, authorUsername, sort);
    }

    @Override
    public Post create(Post post, User user){
        if (user.isBlocked()){
            throw new UnauthorizedOperationException(BLOCKED_CREATION);
        }

        return postRepository.create(post);
    }

    @Override
    public Post update(Post post, User user){
        Post toBeUpdated = postRepository.getById(post.getId());

        if (toBeUpdated.getAuthor().getId() != user.getId() && !user.isAdmin()){
            throw new UnauthorizedOperationException(UNAUTHORIZED_UPDATE);
        }

        return postRepository.update(post);
    }

    @Override
    public void delete(int id, User user){
        Post toBeDeleted = postRepository.getById(id);

        if (toBeDeleted.getAuthor().getId() != user.getId() && !user.isAdmin()){
            throw new UnauthorizedOperationException(UNAUTHORIZED_DELETE);
        }

        postRepository.delete(id);
    }
}
