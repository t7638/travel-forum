package com.team11.travelforum.services.contracts;

import com.team11.travelforum.models.Tag;

import java.util.List;

public interface TagService {
    List<Tag> getAllTags();

    Tag getById(int id);

    Tag getByName(String name);

    void create(Tag tag);

    void update(Tag tag);
}
