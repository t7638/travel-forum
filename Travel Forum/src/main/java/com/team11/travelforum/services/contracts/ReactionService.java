package com.team11.travelforum.services.contracts;

import com.team11.travelforum.models.Reaction;
import com.team11.travelforum.models.User;

import java.util.List;

public interface ReactionService {
    List<Reaction> getAll(int postId);

    Reaction getByUser(int postId, int userId);

    Reaction create(int postId, boolean reactionType, User user);

    Reaction update(Reaction reaction);

    void delete(int postId, int reactionId);
}
