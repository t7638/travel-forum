package com.team11.travelforum.services;

import com.team11.travelforum.models.Avatar;
import com.team11.travelforum.repositories.contracts.AvatarRepository;
import com.team11.travelforum.services.contracts.AvatarService;
import org.springframework.stereotype.Service;

@Service
public class AvatarServiceImpl implements AvatarService {

    private final AvatarRepository avatarRepository;

    public AvatarServiceImpl(AvatarRepository avatarRepository) {
        this.avatarRepository = avatarRepository;
    }

    @Override
    public Avatar getById(int id) {
        return avatarRepository.getById(id);
    }
}
