package com.team11.travelforum.services.contracts;

import com.team11.travelforum.models.Avatar;

public interface AvatarService {
    Avatar getById(int id);
}
