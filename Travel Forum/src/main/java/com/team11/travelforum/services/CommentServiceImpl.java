package com.team11.travelforum.services;

import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.Comment;
import com.team11.travelforum.models.User;
import com.team11.travelforum.repositories.contracts.CommentRepository;
import com.team11.travelforum.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private static final String UNAUTHORIZED_CREATE = "Blocked users can't create comments!";
    public static final String UNAUTHORIZED_UPDATE = "A comment can be modified only by its author!";
    private static final String UNAUTHORIZED_DELETE = "A comment can be deleted only by its author or an admin!";

    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> getAllComments(int postId) {
        return commentRepository.getAllComments(postId);
    }

    @Override
    public Comment getCommentById(int postId, int id) {
        return commentRepository.getCommentById(postId, id);
    }

    @Override
    public List<Comment> getCommentsByAuthor(int postId, String username) {
        return commentRepository.getCommentsByAuthor(postId, username);
    }

    @Override
    public Comment createComment(int postId, Comment comment, User user) {
        if (user.isBlocked()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_CREATE);
        }
        commentRepository.createComment(postId, comment);
        return comment;
    }

    @Override
    public void updateComment(int postId, int id, String content, User user) {
        Comment commentToUpdate = getCommentById(postId, id);
        if (commentToUpdate.getAuthor().getId() != user.getId()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_UPDATE);
        }
        commentToUpdate.setContent(content);
        commentRepository.updateComment(postId,commentToUpdate);
    }

    @Override
    public void deleteComment(int postId, int id, User user) {
        Comment toBeDeleted = commentRepository.getCommentById(postId, id);

        if (toBeDeleted.getAuthor().getId() != user.getId() && !user.isAdmin()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_DELETE);
        }
        commentRepository.delete(postId, id, user);
    }

}