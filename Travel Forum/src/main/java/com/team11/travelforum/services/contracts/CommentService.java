package com.team11.travelforum.services.contracts;

import com.team11.travelforum.models.Comment;
import com.team11.travelforum.models.User;

import java.util.List;

public interface CommentService {
    List<Comment> getAllComments(int postId);

    Comment getCommentById(int postId, int id);

    List<Comment> getCommentsByAuthor(int postId, String username);

    Comment createComment(int postId, Comment comment, User user);

    void updateComment(int postId, int id, String content, User user);

    void deleteComment(int postId, int id, User user);
}
