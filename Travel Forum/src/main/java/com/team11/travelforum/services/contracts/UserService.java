package com.team11.travelforum.services.contracts;

import com.team11.travelforum.models.User;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAllUsers();

    BigInteger getUsersCount();

    User getUserById(int id);

    User getByUsername( String username);

    void createUser(User user);

    void updateUser(User user, int id, User userToUpdate);

    void blockUser(int id, User user);

    void delete(int id, User user);

    void promoteToAdmin(int id, User user);

    List<User> search(Optional<String> search, User user);

}
