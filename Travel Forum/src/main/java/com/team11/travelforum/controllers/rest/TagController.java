package com.team11.travelforum.controllers.rest;

import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.models.Tag;
import com.team11.travelforum.models.dtos.TagDTO;
import com.team11.travelforum.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/forum/tags")
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping
    public List<Tag> getAllTags(){
        return tagService.getAllTags();
    }

    @GetMapping("/{id}")
    public Tag getTagById(@PathVariable int id){
        try {
            return tagService.getById(id);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Tag createTag(@Valid @RequestBody TagDTO tagDTO){
        Tag tag = new Tag();
        tag.setName(tagDTO.getName());
        try {
            tagService.create(tag);
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return tag;
    }

    @PutMapping()
    public Tag updateTag(@Valid @RequestBody Tag tag){
        try {
            tagService.update(tag);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return tag;
    }
}
