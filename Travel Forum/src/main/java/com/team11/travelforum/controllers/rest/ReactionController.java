package com.team11.travelforum.controllers.rest;

import com.team11.travelforum.controllers.AuthenticationHelper;
import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.Reaction;
import com.team11.travelforum.models.User;
import com.team11.travelforum.models.dtos.ReactionDTO;
import com.team11.travelforum.services.contracts.PostService;
import com.team11.travelforum.services.contracts.ReactionService;
import com.team11.travelforum.utils.mappers.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/forum/posts/{id}")
public class ReactionController {

    private final ReactionService reactionService;
    private final PostService postService;
    private final PostMapper postMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ReactionController(ReactionService reactionService, PostService postService,
                              PostMapper postMapper, AuthenticationHelper authenticationHelper) {
        this.reactionService = reactionService;
        this.postService = postService;
        this.postMapper = postMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/likes")
    public List<ReactionDTO> getAllLikes(@PathVariable int id){
        try{
            postService.getById(id);
            return reactionService.getAll(id).stream()
                    .filter(Reaction::getReaction)
                    .map(postMapper::objectToDto)
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/dislikes")
    public List<ReactionDTO> getAllDislikes(@PathVariable int id){
        try{
            postService.getById(id);
            return reactionService.getAll(id).stream()
                    .filter(reaction -> !reaction.getReaction())
                    .map(postMapper::objectToDto)
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/like")
    public Reaction like(@PathVariable int id, @RequestHeader HttpHeaders headers){
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return reactionService.create(id,true, user);
        }catch (UnauthorizedOperationException u){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (DuplicateEntityException d){
            throw new ResponseStatusException(HttpStatus.OK, d.getMessage());
        }
    }

    @PostMapping("/dislike")
    public Reaction dislike(@PathVariable int id, @RequestHeader HttpHeaders headers){
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return reactionService.create(id,false, user);
        }catch (UnauthorizedOperationException u){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (DuplicateEntityException d){
            throw new ResponseStatusException(HttpStatus.OK, d.getMessage());
        }
    }

}