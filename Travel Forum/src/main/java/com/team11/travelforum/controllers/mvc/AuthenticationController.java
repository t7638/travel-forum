package com.team11.travelforum.controllers.mvc;

import com.team11.travelforum.controllers.AuthenticationHelper;
import com.team11.travelforum.exceptions.AuthenticationFailureException;
import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.models.User;
import com.team11.travelforum.models.dtos.LoginDto;
import com.team11.travelforum.models.dtos.UserDTOIn;
import com.team11.travelforum.services.contracts.UserService;
import com.team11.travelforum.utils.mappers.UserMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")

public class AuthenticationController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;


    public AuthenticationController(UserService userService,
                                    AuthenticationHelper authenticationHelper,
                                    UserMapper userMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult errors,
                              HttpSession session) {
        if (errors.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            User loggedUser = userService.getByUsername(login.getUsername());
            session.setAttribute("currentUser", loggedUser);
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            errors.rejectValue("username", "auth.error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/signin")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new UserDTOIn());
        return "sign-in";
    }

    @PostMapping("/signin")
    public String handleRegister(@Valid @ModelAttribute("register") UserDTOIn register,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "sign-in";
        }

        if (!register.getPassword().equals(register.getConfirmedPassword())) {
            bindingResult.rejectValue("confirmedPassword", "password_error",
                    "Password confirmation should match password.");
            return "sign-in";
        }

        try {
            User user = userMapper.dtoToObject(register);
            userService.createUser(user);
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            String[] exceptionMessage = e.getMessage().split(" ");
            String fieldName = exceptionMessage[2];
            bindingResult.rejectValue(fieldName, "username_error", e.getMessage());
            return "sign-in";
        }
    }

}
