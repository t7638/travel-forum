package com.team11.travelforum.controllers.mvc;

import com.team11.travelforum.controllers.AuthenticationHelper;
import com.team11.travelforum.exceptions.AuthenticationFailureException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.User;
import com.team11.travelforum.models.dtos.*;
import com.team11.travelforum.services.contracts.PostService;
import com.team11.travelforum.services.contracts.UserService;
import com.team11.travelforum.utils.mappers.PostMapper;
import com.team11.travelforum.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final PostService postService;
    private final PostMapper postMapper;

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMvcController(PostService postService,
                             PostMapper postMapper, UserService userService,
                             UserMapper userMapper,
                             AuthenticationHelper authenticationHelper) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticatedUser")
    public User populateAuthenticatedUser(HttpSession session) {
        return (User) session.getAttribute("currentUser");
    }

    @GetMapping
    public String showAllUsers(Model model) {

        List<UserDTOOutForAdmins> allUsers = userService.getAllUsers().stream()
                .map(userMapper::userToUserDtoForAdmins)
                .collect(Collectors.toList());
        model.addAttribute("users",allUsers );

        return "users";
    }

    @PostMapping("/search")
    public String searchUser(Model model, HttpSession session, @ModelAttribute ("search")String search) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        List<User> users = userService.search(Optional.of(search), user);

        List<UserDTOOutForAdmins> searchedUsers = users.stream()
                .map(userMapper::userToUserDtoForAdmins)
                .collect(Collectors.toList());
        //model.addAttribute("user", user);
        model.addAttribute("users", searchedUsers);
        return "users";

    }

    @PostMapping("/{id}/block")
    public String blockUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            userService.blockUser(id, user);
            return "redirect:/users/{id}";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found"; /* No option for the button if not Admin or user is admin*/
        }

    }

    @PostMapping("/{id}/promote")
    public String makeAdmin(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.promoteToAdmin(id, user);
            return "redirect:/users/{id}";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found"; /* No option for the button if not Admin or user is admin*/
        }

    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        User user = userService.getUserById(id);

        if (user.isDeleted()){
            model.addAttribute("error", "This user does not exist or has been deleted.");
            return "not-found";
        }

        try {
            List<PostOutDTO> userPosts = postService.filter(Optional.empty(), Optional.empty(), Optional.ofNullable(user.getUsername()), Optional.empty())
                    .stream()
                    .map(postMapper::objectToDto)
                    .collect(Collectors.toList());

            model.addAttribute("user", user);
            model.addAttribute("userPosts", userPosts);
            return "user-profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getUserById(id);
            UpdateUserDTO updateUserDTO = userMapper.objectToUpdateDto(user);
            model.addAttribute("userToUpdate", updateUserDTO);
            return "user-update";
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("userToUpdate") UpdateUserDTO updateUserDTO,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("user", user);

        model.addAttribute("action", "update");
        model.addAttribute("Id", userService.getUserById(updateUserDTO.getId()));

        if (bindingResult.hasErrors()) {
            return "user-update";
        }

        try {
            User userToUpdate = userMapper.dtoToObject(updateUserDTO);
            userService.updateUser(user, id, userToUpdate);
            return "redirect:/users/{id}";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
    }
    @GetMapping("/{id}/updatepassword")
    public String showEditPasswordUserPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getUserById(id);
            UpdatePasswordDto updatePasswordDto = userMapper.objectToPasswordDto(user);
            model.addAttribute("userToUpdate", updatePasswordDto);
            return "password-update";
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
    @PostMapping("/{id}/updatepassword")
    public String updateUserPassword(@PathVariable int id,
                                     @Valid @ModelAttribute("userToUpdate") UpdatePasswordDto updatePasswordDto,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("user", user);
        if (!updatePasswordDto.getPassword().equals(updatePasswordDto.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error",
                    "Password confirmation should match password.");
            return "password-update";
        }
        model.addAttribute("action", "update");
        model.addAttribute("Id", userService.getUserById(updatePasswordDto.getId()));

        if (bindingResult.hasErrors()) {
            return "password-update";
        }

        try {
            User userToUpdate = userMapper.dtoToObject(updatePasswordDto);
            userService.updateUser(user, id, userToUpdate);
            return "redirect:/users/{id}";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.delete(id, user);
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }

        return "redirect:/auth/logout";
    }

}
