package com.team11.travelforum.controllers.rest;

import com.team11.travelforum.controllers.AuthenticationHelper;
import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.PhoneNumber;
import com.team11.travelforum.models.User;
import com.team11.travelforum.models.dtos.PhoneNumberDTO;
import com.team11.travelforum.models.dtos.PhoneNumberDTOOut;
import com.team11.travelforum.services.contracts.PhoneNumberService;
import com.team11.travelforum.utils.mappers.PhoneNumberMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/forum/users/phonenumbers")
public class PhoneNumberController {

    private final PhoneNumberService phoneNumberService;
    private final PhoneNumberMapper phoneNumberMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PhoneNumberController(PhoneNumberService phoneNumberService, PhoneNumberMapper phoneNumberMapper,
                                 AuthenticationHelper authenticationHelper) {
        this.phoneNumberService = phoneNumberService;
        this.phoneNumberMapper = phoneNumberMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<PhoneNumberDTOOut> getAll(@RequestHeader HttpHeaders headers){
        try{
            User user = authenticationHelper.tryGetUser(headers);

            return phoneNumberService.getAll(user).stream()
                    .map(phoneNumberMapper::objectToDto)
                    .collect(Collectors.toList());

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public void create(@RequestHeader HttpHeaders headers, @Valid @RequestBody PhoneNumberDTO phoneNumberDTO){
        try{
            User user = authenticationHelper.tryGetUser(headers);
                    PhoneNumber phoneNumber= phoneNumberMapper.dtoToObject(phoneNumberDTO);
            phoneNumberService.create(phoneNumber, user);
        }catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
