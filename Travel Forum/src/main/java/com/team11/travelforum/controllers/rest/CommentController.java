package com.team11.travelforum.controllers.rest;

import com.team11.travelforum.controllers.AuthenticationHelper;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.Comment;
import com.team11.travelforum.models.User;
import com.team11.travelforum.models.dtos.CommentDTO;
import com.team11.travelforum.models.dtos.CommentDTOOut;
import com.team11.travelforum.services.contracts.CommentService;
import com.team11.travelforum.utils.mappers.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("forum/posts/{postId}/comments")
public class CommentController {

    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CommentController(CommentService commentService, CommentMapper commentMapper, AuthenticationHelper authenticationHelper) {
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<CommentDTOOut> getAllComments(@PathVariable int postId) {
        try {
            return commentService.getAllComments(postId)
                    .stream()
                    .map(commentMapper::objectToDto)
                    .collect(Collectors.toList());
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public CommentDTOOut getCommentById(@PathVariable int postId, @PathVariable int id) {
        try {
            Comment comment =commentService.getCommentById(postId, id);
            return commentMapper.objectToDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Comment createComments(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                                  @Valid @RequestBody CommentDTO commentDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.dtoToObject(commentDTO, user);
            return commentService.createComment(postId, comment, user);
        }  catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public void updateComment(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                              @PathVariable int id, @Valid @RequestBody String content) {

        try {
            User user = authenticationHelper.tryGetUser(headers);

            commentService.updateComment(postId, id, content, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteComment(@RequestHeader HttpHeaders headers, @PathVariable int postId, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            commentService.deleteComment(postId, id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
