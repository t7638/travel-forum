package com.team11.travelforum.controllers.rest;

import com.team11.travelforum.controllers.AuthenticationHelper;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.User;
import com.team11.travelforum.models.dtos.CreatePostDTO;
import com.team11.travelforum.models.dtos.PostOutDTO;
import com.team11.travelforum.models.dtos.UpdatePostDTO;
import com.team11.travelforum.services.contracts.PostService;
import com.team11.travelforum.utils.mappers.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/forum/posts")
public class PostController {

    private final PostService postService;
    private final PostMapper postMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PostController(PostService postService, PostMapper postMapper, AuthenticationHelper authenticationHelper) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<PostOutDTO> getAllPosts(
            @RequestParam(required = false) Optional<String> sort,
            @RequestHeader HttpHeaders headers) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(headers);
            return postService.getAllPosts(sort, Optional.ofNullable(user))
                    .stream()
                    .map(postMapper::objectToDto)
                    .collect(Collectors.toList());
        }catch (ResponseStatusException r){
            try {
                return postService.getAllPosts(sort, Optional.empty())
                        .stream()
                        .map(postMapper::objectToDto)
                        .collect(Collectors.toList());
            }catch (UnauthorizedOperationException u){
                throw new ResponseStatusException(HttpStatus.CONFLICT, u.getMessage());
            }
        }
    }

    @GetMapping("/filter")
    public List<PostOutDTO> filter(@RequestHeader HttpHeaders headers,
                                   @RequestParam(required = false) Optional<String> title,
                                   @RequestParam(required = false) Optional<String> tagName,
                                   @RequestParam(required = false) Optional<String> authorUsername,
                                   @RequestParam(required = false) Optional<String> sort){
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return postService.filter(title, tagName, authorUsername, sort)
                    .stream()
                    .map(postMapper::objectToDto)
                    .collect(Collectors.toList());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnsupportedOperationException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @GetMapping("/{id}")
    public PostOutDTO getById(@PathVariable int id) {
        try {
            Post post = postService.getById(id);
            return postMapper.objectToDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Post create(@RequestHeader HttpHeaders headers, @Valid @RequestBody CreatePostDTO createPostDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.dtoToObject(createPostDTO, user);
            return postService.create(post, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping()
    public Post update(@RequestHeader HttpHeaders headers, @Valid @RequestBody UpdatePostDTO updatePostDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.dtoToObject(updatePostDTO);
            return postService.update(post, user);
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            postService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
