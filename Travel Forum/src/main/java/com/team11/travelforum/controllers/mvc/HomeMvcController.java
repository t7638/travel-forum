package com.team11.travelforum.controllers.mvc;

import com.team11.travelforum.controllers.AuthenticationHelper;
import com.team11.travelforum.exceptions.AuthenticationFailureException;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.User;
import com.team11.travelforum.models.dtos.PostOutDTO;
import com.team11.travelforum.services.contracts.PostService;
import com.team11.travelforum.services.contracts.UserService;
import com.team11.travelforum.utils.mappers.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final PostService postService;
    private final UserService userService;
    private final PostMapper postMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public HomeMvcController(PostService postService, UserService userService, PostMapper postMapper,
                             AuthenticationHelper authenticationHelper) {
        this.postService = postService;
        this.userService = userService;
        this.postMapper = postMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticatedUser")
    public Object populateAuthenticatedUser(HttpSession session){
        return session.getAttribute("currentUser");
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session) {
        model.addAttribute("orderBy", "comments");
        return  homePage(model, session, "comments_desc");
    }

    @GetMapping("/latest")
    public String showHomePageLatest(Model model, HttpSession session) {
        model.addAttribute("orderBy", "date");
        return  homePage(model, session, "creationDate_desc");
    }

    private String homePage(Model model, HttpSession session, String sort){
        Optional<User> loggedUser;
        try {
            loggedUser = Optional.ofNullable(authenticationHelper.tryGetUser(session));
        }catch (AuthenticationFailureException a){
            loggedUser = Optional.empty();
        }

        List<Post> objectPosts = postService.getAllPosts(Optional.of(sort), loggedUser);
        List<PostOutDTO> posts = objectPosts.stream()
                .map(postMapper::objectToDto)
                .limit(10)
                .collect(Collectors.toList());
        model.addAttribute("posts", posts);
        model.addAttribute("usersCount", userService.getUsersCount());
        model.addAttribute("postsCount", postService.getPostsCount());
        return "index";
    }

}
