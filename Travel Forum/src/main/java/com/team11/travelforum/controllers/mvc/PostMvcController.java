package com.team11.travelforum.controllers.mvc;

import com.team11.travelforum.controllers.AuthenticationHelper;
import com.team11.travelforum.exceptions.AuthenticationFailureException;
import com.team11.travelforum.exceptions.DuplicateEntityException;
import com.team11.travelforum.exceptions.EntityNotFoundException;
import com.team11.travelforum.exceptions.UnauthorizedOperationException;
import com.team11.travelforum.models.Comment;
import com.team11.travelforum.models.Post;
import com.team11.travelforum.models.Reaction;
import com.team11.travelforum.models.User;
import com.team11.travelforum.models.dtos.*;
import com.team11.travelforum.models.enums.PostSortOptions;
import com.team11.travelforum.services.contracts.CommentService;
import com.team11.travelforum.services.contracts.PostService;
import com.team11.travelforum.services.contracts.ReactionService;
import com.team11.travelforum.utils.mappers.CommentMapper;
import com.team11.travelforum.utils.mappers.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Controller
@RequestMapping("/posts")
public class PostMvcController {

    private static final String REDIRECT = "redirect:/posts/%d";

    private final PostService postService;
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;
    private final CommentService commentService;
    private final AuthenticationHelper authenticationHelper;
    private final ReactionService reactionService;

    @Autowired
    public PostMvcController(PostService postService, PostMapper postMapper,
                             CommentMapper commentMapper, CommentService commentService,
                             AuthenticationHelper authenticationHelper, ReactionService reactionService) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.commentMapper = commentMapper;
        this.commentService = commentService;
        this.authenticationHelper = authenticationHelper;
        this.reactionService = reactionService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticatedUser")
    public User populateAuthenticatedUser(HttpSession session){
        return (User) session.getAttribute("currentUser");
    }

    @ModelAttribute("sortPostOptions")
    public PostSortOptions[] populateSortOptions() {
        return PostSortOptions.values();
    }

    @ModelAttribute("filterDto")
    public FilterPostDto populateFilterDto() {
        return new FilterPostDto();
    }

    @GetMapping
    public String showAllPosts(Model model, HttpSession session){
        Optional<User> loggedUser;
        try {
            loggedUser = Optional.ofNullable(authenticationHelper.tryGetUser(session));
        }catch (AuthenticationFailureException a){
            loggedUser = Optional.empty();
        }

        List<Post> objectPosts = postService.getAllPosts(Optional.empty(), loggedUser);
        List<PostOutDTO> posts = objectPosts.stream()
                .map(postMapper::objectToDto)
                .collect(Collectors.toList());
        model.addAttribute("posts", posts);

        return "posts";
    }

    @PostMapping("/filter")
    public String filter(@ModelAttribute("filterDto") FilterPostDto filterPostDto, Model model){
        model.addAttribute("searchTerm", filterPostDto.getTitle());

        if (filterPostDto.getTitle() != null && filterPostDto.getTitle().startsWith("#")) {
            filterPostDto.setTagName(filterPostDto.getTitle().substring(1));
            filterPostDto.setTitle("");
        }

        List<Post> filtered = postService.filter(
                Optional.ofNullable(filterPostDto.getTitle().isBlank() ? null : filterPostDto.getTitle()),
                Optional.ofNullable(filterPostDto.getTagName() == null ? null : filterPostDto.getTagName()),
                Optional.ofNullable(filterPostDto.getAuthorUsername()),
                Optional.ofNullable(filterPostDto.getSort().equals("Unsorted") ?
                        null : PostSortOptions.valueOfPreview(filterPostDto.getSort()).getQuery()));

        List<PostOutDTO> posts = filtered.stream()
                .map(postMapper::objectToDto)
                .collect(Collectors.toList());
        model.addAttribute("posts", posts);
        return "posts";
    }

    @GetMapping("/filter/tag/{tagText}")
    public String filterByTag(@PathVariable String tagText, Model model){
        List<Post> objectPosts = postService.filter(Optional.empty(), Optional.ofNullable(tagText),
                Optional.empty(), Optional.empty());
        List<PostOutDTO> posts = objectPosts.stream()
                .map(postMapper::objectToDto)
                .collect(Collectors.toList());
        model.addAttribute("searchTerm","#" + tagText);
        model.addAttribute("posts", posts);
        return "posts";
    }

    @GetMapping("/{id}")
    public String showSinglePost(@PathVariable int id, Model model, HttpSession session){
        try {
            Post currentPost = postService.getById(id);
            PostOutDTO post = postMapper.objectToDto(currentPost);
            model.addAttribute("post", post);
            model.addAttribute("comments", post.getCommentsOutDTOs());
            model.addAttribute("commentDto", new CommentDTO());
            model.addAttribute("currUserReaction", getReactionHelper(session, id));
            return "post";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showCreatePostPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if(user.isBlocked()){
            model.addAttribute("error", "Blocked users can't create posts.");
            return "unauthorized";
        }

        model.addAttribute("authorId", user.getId());
        model.addAttribute("action","create");
        model.addAttribute("post", new CreatePostDTO());
        return "post-new";
    }

    @PostMapping("/new")
    public String createPost(@Valid @ModelAttribute("post") CreatePostDTO createPostDTO,
                             BindingResult errors, Model model, HttpSession session){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        }catch (AuthenticationFailureException a){
            return "redirect:/auth/login";
        }

        model.addAttribute("authorId", user.getId());
        model.addAttribute("action","create");
        if (errors.hasErrors()){
            return "post-new";
        }

        try {
            Post post = postMapper.dtoToObject(createPostDTO, user);
            postService.create(post, user);
            return format(REDIRECT, post.getId());
        }catch (UnauthorizedOperationException u){
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdatePostPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Post post = postService.getById(id);
            UpdatePostDTO updatePostDTO = postMapper.objectToUpdateDto(post);
            model.addAttribute("post", updatePostDTO);
            model.addAttribute("action","update");
            model.addAttribute("authorId", post.getAuthor().getId());
            return "post-update";
        } catch (UnauthorizedOperationException u){
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updatePost(@PathVariable int id,
                             @Valid @ModelAttribute("post") UpdatePostDTO updatePostDTO,
                             BindingResult errors,
                             Model model, HttpSession session){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        }catch (AuthenticationFailureException a){
            return "redirect:/auth/login";
        }

        model.addAttribute("action","update");
        model.addAttribute("authorId", postService.getById(updatePostDTO.getId()).getAuthor().getId());
        if (errors.hasErrors()){
            return "post-update";
        }

        try {
            Post post = postMapper.dtoToObject(updatePostDTO);
            postService.update(post, user);
            return format(REDIRECT, id);
        }catch (IllegalArgumentException i){
            errors.rejectValue("tagNames", "tags-invalid", i.getMessage());
            return "post-update";
        }
        catch (UnauthorizedOperationException u){
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/delete")
    public String deletePost(@PathVariable int id, HttpSession session, Model model){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        }catch (AuthenticationFailureException a){
            return "redirect:/auth/login";
        }

        try {
            postService.delete(id, user);
        }catch (UnauthorizedOperationException u){
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }

        return "redirect:/";
    }

    @PostMapping("/{postId}/comments/new")
    public String createComment(@PathVariable int postId, @Valid @ModelAttribute("commentDto") CommentDTO newComment,
                                BindingResult errors, HttpSession session, Model model){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if(user.isBlocked()){
            model.addAttribute("error", "Blocked users can't create comments.");
            return "unauthorized";
        }

        if (errors.hasErrors()) {
            return format(REDIRECT, postId);
        }

        try {
            Comment comment = commentMapper.dtoToObject(newComment, user);
            commentService.createComment(postId, comment, user);
            return format(REDIRECT, postId);
        }catch (UnauthorizedOperationException u){
            errors.rejectValue("content", "user.blocked", u.getMessage());
            return format(REDIRECT, postId);
        }
    }

    @GetMapping("/{postId}/comments/update/{commentId}")
    public String showUpdateCommentPage(@PathVariable int postId, @PathVariable int commentId,
                                        Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Comment comment = commentService.getCommentById(postId, commentId);
            CommentDTO commentDTO = commentMapper.forUpdate(comment);
            model.addAttribute("commentID", commentId);
            model.addAttribute("postID", postId);
            model.addAttribute("commentDto", commentDTO);

            if (user.getId() != comment.getAuthor().getId()){
                model.addAttribute("error", "A comment can be modified only by its author!");
                return "unauthorized";
            }

            return "comment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{postId}/comments/update/{commentId}")
    public String updateComment(@PathVariable int postId, @PathVariable int commentId,
                                @Valid @ModelAttribute("commentDto") CommentDTO updateComment,
                                BindingResult errors, HttpSession session, Model model){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "comment-update";
        }

        try {
            commentService.updateComment(postId, commentId, updateComment.getContent(), user);
        }catch (UnauthorizedOperationException u){
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }

        return format(REDIRECT, postId);
    }
    
    @GetMapping("/{postId}/comments/delete/{commentId}")
    public String deleteComment(@PathVariable int postId, @PathVariable int commentId, HttpSession session, Model model){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            commentService.deleteComment(postId, commentId, user);
            return format(REDIRECT, postId);
        }catch (AuthenticationFailureException a){
            return "redirect:/auth/login";
        }catch (UnauthorizedOperationException u){
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }catch (EntityNotFoundException e){
            return "not-found";
        }
    }

    @GetMapping("/{postId}/like")
    public String likePost(@PathVariable int postId, HttpSession session, Model model){
        return reactHelper(postId, session, model ,true);
    }

    @GetMapping("/{postId}/dislike")
    public String dislikePost(@PathVariable int postId, HttpSession session, Model model){
        return reactHelper(postId, session, model ,false);
    }

    private String reactHelper(int postId, HttpSession session, Model model, Boolean reactionType) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        }catch (AuthenticationFailureException e){
            return "redirect:/auth/login";
        }

        try {
            reactionService.create(postId, reactionType, user);
            return format(REDIRECT, postId);
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }catch (DuplicateEntityException d){
            return format(REDIRECT, postId);
        }
    }

    private String getReactionHelper(HttpSession session, int postId){
        String reaction;
        try {
            User user = authenticationHelper.tryGetUser(session);
            try {
                Reaction currReaction = reactionService.getByUser(postId, user.getId());
                if (currReaction.getReaction()){
                    reaction = "like";
                }else {
                    reaction = "dislike";
                }
            }catch (EntityNotFoundException e){
                reaction = "none";
            }
        } catch (AuthenticationFailureException e) {
            reaction = "none";
        }

        return reaction;
    }
}
