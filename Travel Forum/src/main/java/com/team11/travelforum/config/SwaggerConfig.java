package com.team11.travelforum.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket configuration(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.team11.travelforum.controllers.rest"))
                .paths(regex("/forum.*"))
                .build()
                .apiInfo(apiDetails());
    }

    private ApiInfo apiDetails(){
        return new ApiInfo(
                "Travel Forum",
                "Forum for learning and sharing traveling experience",
                "1.0",
                "Free to Use",
                new springfox.documentation.service.Contact("Lilia Georgieva and Ivo Bankov", "", ""),
                "Developed by: Lilia Georgieva and Ivo Bankov",
                "",
                Collections.emptyList()
        );
    }
}
