package com.team11.travelforum.models.dtos;

import java.time.LocalDate;
import java.util.List;

public class PostOutDTO {

    private int id;

    private int authorId;

    private String authorUsername;

    private String title;

    private String content;

    private List<CommentDTOOut> commentsOutDTOs;

    private long likes;

    private long dislikes;

    private List<String> tagNames;

    private LocalDate creationDate;

    public PostOutDTO() {
    }

    public PostOutDTO(int id, int authorId, String authorUsername, String title,
                      String content, List<CommentDTOOut> commentsOutDTOs, long likes,
                      long dislikes, List<String> tagNames, LocalDate creationDate) {
        this.id = id;
        this.authorId = authorId;
        this.authorUsername = authorUsername;
        this.title = title;
        this.content = content;
        this.commentsOutDTOs = commentsOutDTOs;
        this.likes = likes;
        this.dislikes = dislikes;
        this.tagNames = tagNames;
        this.creationDate = creationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<CommentDTOOut> getCommentsOutDTOs() {
        return commentsOutDTOs;
    }

    public void setCommentsOutDTOs(List<CommentDTOOut> commentsOutDTOs) {
        this.commentsOutDTOs = commentsOutDTOs;
    }

    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }

    public long getDislikes() {
        return dislikes;
    }

    public void setDislikes(long dislikes) {
        this.dislikes = dislikes;
    }

    public List<String> getTagNames() {
        return tagNames;
    }

    public void setTagNames(List<String> tagNames) {
        this.tagNames = tagNames;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }
}
