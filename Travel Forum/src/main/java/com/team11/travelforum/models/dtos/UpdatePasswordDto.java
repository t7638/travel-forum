package com.team11.travelforum.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class UpdatePasswordDto {
    @Positive(message = "Id should be positive")
    private int id;

    @NotNull(message = "Password can't be empty")
    @Size(min = 8, message = "Password should be longer than 8 symbols")
    private String password;

    @NotNull(message = "Password conformation can't be empty")
    @Size(min = 8, message = "Password should be longer than 8 symbols")
    private String passwordConfirm;

    public UpdatePasswordDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
