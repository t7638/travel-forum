package com.team11.travelforum.models.dtos;

public class ReactionDTO {

    private String username;

    private boolean reaction;

    public ReactionDTO() {
    }

    public ReactionDTO(String username, boolean reaction) {
        this.username = username;
        this.reaction = reaction;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isReaction() {
        return reaction;
    }

    public void setReaction(boolean reaction) {
        this.reaction = reaction;
    }
}
