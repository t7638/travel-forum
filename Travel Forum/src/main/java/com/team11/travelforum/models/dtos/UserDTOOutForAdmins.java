package com.team11.travelforum.models.dtos;

import com.team11.travelforum.models.Avatar;

import java.util.Objects;

public class UserDTOOutForAdmins {

    private int id;

    private String firstName;

    private String lastName;

    private String email;

    private String username;

    private String phoneNumber;

    private boolean isAdmin ;

    private boolean isBlock ;

    private boolean isDeleted;

    private Avatar avatar;


    public UserDTOOutForAdmins() {
    }

    public UserDTOOutForAdmins(int id, String firstName, String lastName, String email, String username, String phoneNumber, boolean isAdmin, boolean isBlock, Avatar avatar) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.isAdmin = isAdmin;
        this.isBlock = isBlock;
        this.avatar = avatar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isBlock() {
        return isBlock;
    }

    public void setBlock(boolean block) {
        isBlock = block;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTOOutForAdmins user = (UserDTOOutForAdmins) o;
        return id == user.getId()&& username.equals(user.getUsername())&& email.equals(user.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,username,email);
    }

}
