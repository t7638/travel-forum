package com.team11.travelforum.models.dtos;

public class PhoneNumberDTOOut {

    private String username;
    private String firstName;
    private String lastName;
    private String number;

    public PhoneNumberDTOOut() {
    }

    public PhoneNumberDTOOut(String username, String firstName, String lastName, String number) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.number = number;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
