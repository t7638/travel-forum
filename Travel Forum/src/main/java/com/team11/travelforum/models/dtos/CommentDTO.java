package com.team11.travelforum.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CommentDTO {

    @NotNull(message = "Content can't be empty")
    @Size(min = 1, max = 2048, message = "Content should be between 1 and 2048 symbols")
    private String content;

    public CommentDTO() {
    }

    public CommentDTO(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
