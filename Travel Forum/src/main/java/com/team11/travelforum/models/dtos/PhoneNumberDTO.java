package com.team11.travelforum.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class PhoneNumberDTO {

    @NotNull
    @Size(min = 10, message = "Phone number should be longer than 10 digits")
    @Pattern(regexp = "[0-9+]+", message = "Phone number can contain only digits")
    private String phoneNumber;

    @NotNull
    private int userId;

    public PhoneNumberDTO() {
    }

    public PhoneNumberDTO(String phoneNumber, int userId) {
        this.phoneNumber = phoneNumber;
        this.userId = userId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
