package com.team11.travelforum.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum PostSortOptions {

    TITLE_ASC("Title ⬆", " title "),
    TITLE_DESC("Title ⬇", " title_desc "),
    REACTIONS_ASC("Reactions ⬆", " reactions "),
    REACTIONS_DESC("Reactions ⬇", " reactions_desc "),
    COMMENTS_ASC("Comments ⬆", " comments "),
    COMMENTS_DESC("Comments ⬇", " comments_desc "),
    DATE_ASC("Date ⬆", " creationDate "),
    DATE_DESC("Date ⬇", " creationDate_desc ");

    private final String preview;
    private final String query;

    private static final Map<String, PostSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (PostSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    PostSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static PostSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }

}
