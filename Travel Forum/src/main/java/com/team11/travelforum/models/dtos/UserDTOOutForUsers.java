package com.team11.travelforum.models.dtos;

public class UserDTOOutForUsers {

private String username;
private boolean admin;

    public UserDTOOutForUsers() {
    }

    public UserDTOOutForUsers(String username, boolean admin) {
        this.username = username;
        this.admin = admin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }


}
