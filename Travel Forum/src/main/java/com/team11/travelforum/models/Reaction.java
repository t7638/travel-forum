package com.team11.travelforum.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "reactions")
public class Reaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reaction_id")
    private int id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "reaction_type")
    private boolean reaction;

    public Reaction() {
    }

    public Reaction(int id, User user, boolean reaction) {
        this.id = id;
        this.user = user;
        this.reaction = reaction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean getReaction() {
        return reaction;
    }

    public void setReaction(boolean reaction) {
        this.reaction = reaction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reaction reaction1 = (Reaction) o;
        return id == reaction1.id && reaction == reaction1.reaction && user.equals(reaction1.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, reaction);
    }
}
