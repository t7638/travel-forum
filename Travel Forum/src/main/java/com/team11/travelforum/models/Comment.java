package com.team11.travelforum.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private int id;

    @Column(name = "creation_date")
    @NotNull(message = "Creation date can't be empty")
    private LocalDate creationDate;

    @JoinColumn(name = "author_id")
    @OneToOne
    private User author;

    @Column(name = "content")
    private String content;

    public Comment() {
    }

    public Comment(int id, LocalDate creationDate, User author, String content) {
        this.id = id;
        this.creationDate = creationDate;
        this.author = author;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate date) {
        this.creationDate = date;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id == comment.id && author.equals(comment.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, author);
    }
}
