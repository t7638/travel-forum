package com.team11.travelforum.models.dtos;

import java.time.LocalDate;

public class CommentDTOOut {

    private int comment_id;

    private LocalDate date;

    private String author;

    private String avatarUrl;

    private String content;

    public CommentDTOOut() {

    }

    public CommentDTOOut(int comment_id, LocalDate date, String author, String avatarUrl, String content) {
        this.comment_id=comment_id;
        this.date = date;
        this.author = author;
        this.avatarUrl = avatarUrl;
        this.content = content;
    }

    public int getComment_id() {
        return comment_id;
    }

    public void setComment_id(int comment_id) {
        this.comment_id = comment_id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
