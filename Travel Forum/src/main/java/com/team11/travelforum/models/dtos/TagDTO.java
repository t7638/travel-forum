package com.team11.travelforum.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class TagDTO {

    @NotNull(message = "Tag can't be empty")
    @Size(min = 2, max = 15, message = "Tag should be between 2 and 15 symbols")
    @Pattern(regexp = "[a-z0-9]+", message = "Tag can contain only digits and lowercase characters")
    private String name;

    public TagDTO() {
    }

    public TagDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
