package com.team11.travelforum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TravelForumApplication {

    public static void main(String[] args) {
        SpringApplication.run(TravelForumApplication.class, args);
    }

}
